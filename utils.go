package gozillalib

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	b64 "encoding/base64"

	"github.com/elastic/go-elasticsearch/esapi"

	consul "github.com/hashicorp/consul/api"
	"github.com/streadway/amqp"
	yaml "gopkg.in/yaml.v2"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	ini "github.com/ochinchina/go-ini"

	_ "github.com/influxdata/influxdb1-client" // this is important because of the bug in go mod
	influx "github.com/influxdata/influxdb1-client/v2"
)

// TokenMsg is a fernet token message data
type TokenMsg struct {
	User   User
	Expire int64
}

// MongoConfig contains mongodb server connection info
type MongoConfig struct {
	URL string `json:"url"`
	DB  string `json:"db"`
}

// WebConfig contains server info
type WebConfig struct {
	Port   int    // 80
	Listen string // 0.0.0.0
}

// StorageType defiend kind of storage
type StorageType int

const (
	// LocalStorage use local filesystem
	LocalStorage StorageType = 0
	// SwiftStorage use Openstack Swift
	SwiftStorage StorageType = 1
)

// OpenstackConfig defines openstack swift storage
type OpenstackConfig struct {
	Domain         string `json:"domain"` // domain id
	Region         string `json:"region"`
	Project        string `json:"project"` // tenant id
	AccessKey      string `json:"access" yaml:"access"`
	AccessPassword string `json:"password" yaml:"password"`
	Secret         string `json:"secret"` // Secret key stored at account level for tempurl
}

// StorageConfig defines kind of used storage
type StorageConfig struct {
	Type      StorageType     `json:"type"`
	URL       string          `json:"url"`
	Prefix    string          `json:"prefix"`
	Openstack OpenstackConfig `json:"openstack"`
}

// DebianConfig contains configuration for debian repos
type DebianConfig struct {
	Script string `json:"script"`
	Aptly  string `json:"aptly"` // aptly directory
}

// RpmConfig contains configuration for rpm repos
type RpmConfig struct {
	Script string `json:"script"`
}

// GpgConfig contains gpg key info
type GpgConfig struct {
	Key        string
	Passphrase string
	Pubkeyfile string // Path to pub key file to export it
}

// CASServer define a CAS endpoint
type CASServer struct {
	ID          string
	Customer    string // Link users authenticated via this CAS to an existing customer
	LoginURL    string
	ValidateURL string
}

// AuthConfig contains authentication config parameters
type AuthConfig struct {
	CAS []CASServer
}

// InfluxConfig defines influxdb connection info
type InfluxConfig struct {
	Address  string
	Login    string
	Password string
	Db       string
}

// Config contains goterra configuration
type Config struct {
	Mongo MongoConfig `json:"mongo"`
	URL   string      `json:"url"`
	Proxy string      `json:"proxy"` // nginx web proxy
	Debug int         `json:"debug"` // (de)activate debug [0,1]
	// Secret string
	Fernet   []string      `json:"fernet"`
	Web      WebConfig     `json:"web"`
	Storage  StorageConfig `json:"storage"`
	Consul   string        `json:"consul"`
	Amqp     string        `json:"amqp"`     // rabbitmq url connection "amqp://guest:guest@localhost:5672/"
	Elastic  []string      `json:"elastic"`  // Elasticsearch host addresses
	Influxdb InfluxConfig  `json:"influxdb"` // Influxdb config (optional)
	Debian   DebianConfig
	Rpm      RpmConfig
	Gpg      GpgConfig
	Auth     AuthConfig
}

// Singleton config
//var cfg Config

// ConfigFile config file path
var ConfigFile string

// LoadConfig returns the singleton config object
func LoadConfig() Config {
	/*
		if cfg.loaded {
			return cfg
		}*/
	cfg := Config{
		Web:   WebConfig{Port: 8080},
		Mongo: MongoConfig{URL: "mongodb://localhost:27017/gozilla"},
		Storage: StorageConfig{
			Type: LocalStorage,
		},
	}

	cfgFile := os.Getenv("GOZ_CONFIG")
	if cfgFile != "" {
		ConfigFile = os.Getenv("GOZ_CONFIG")
	}
	if ConfigFile == "" {
		ConfigFile = "gozilla.yml"
	}
	log.Printf("Using config file %s\n", ConfigFile)

	cfgfile, err := ioutil.ReadFile(ConfigFile)
	if err == nil {
		yaml.Unmarshal([]byte(cfgfile), &cfg)
	} else {
		log.Error().Str("config", cfgFile).Msg("Failed to read config file, using defaults and env vars")
	}

	if os.Getenv("GOZ_DEBUG") == "1" {
		cfg.Debug = 1
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	gozURL := os.Getenv("GOZ_URL")
	if gozURL != "" {
		cfg.URL = gozURL
	}

	gozConsul := os.Getenv("GOZ_CONSUL")
	if gozConsul != "" {
		cfg.Consul = gozConsul
	}

	gozAmqp := os.Getenv("GOZ_AMQP")
	if gozAmqp != "" {
		cfg.Amqp = gozAmqp
	}

	gozWebListen := os.Getenv("GOZ_WEB_LISTEN")
	if gozWebListen != "" {
		cfg.Web.Listen = gozWebListen
	}
	gozWebPort := os.Getenv("GOZ_WEB_PORT")
	if gozWebPort != "" {
		port, _ := strconv.Atoi(gozWebPort)
		cfg.Web.Port = port
	}
	gozElastic := os.Getenv("GOZ_ELASTIC")
	if gozElastic != "" {
		cfg.Elastic = strings.Split(gozElastic, ",")
	}
	gozMongoURL := os.Getenv("GOZ_MONGO_URL")
	if gozMongoURL != "" {
		cfg.Mongo.URL = gozMongoURL
	}
	gozMongoDB := os.Getenv("GOZ_MONGO_DB")
	if gozMongoDB != "" {
		cfg.Mongo.DB = gozMongoDB
	}
	gozFernet := os.Getenv("GOZ_FERNET")
	if gozFernet != "" {
		cfg.Fernet = strings.Split(gozFernet, ",")
	}

	gozStorType := os.Getenv("GOZ_STORAGE_TYPE")
	if gozStorType != "" {
		st, _ := strconv.Atoi(gozStorType)
		cfg.Storage.Type = StorageType(st)
	}

	gozStorURL := os.Getenv("GOZ_STORAGE_URL")
	if gozStorURL != "" {
		cfg.Storage.URL = gozStorURL
	}

	gozStorPrefix := os.Getenv("GOZ_STORAGE_PREFIX")
	if gozStorPrefix != "" {
		cfg.Storage.Prefix = gozStorPrefix
	}

	gozOpenstackDomain := os.Getenv("GOZ_STORAGE_OPENSTACK_DOMAIN")
	if gozOpenstackDomain != "" {
		cfg.Storage.Openstack.Domain = gozOpenstackDomain
	}
	gozOpenstackRegion := os.Getenv("GOZ_STORAGE_OPENSTACK_REGION")
	if gozOpenstackRegion != "" {
		cfg.Storage.Openstack.Region = gozOpenstackRegion
	}
	gozOpenstackProject := os.Getenv("GOZ_STORAGE_OPENSTACK_PROJECT")
	if gozOpenstackProject != "" {
		cfg.Storage.Openstack.Project = gozOpenstackProject
	}
	gozOpenstackAccess := os.Getenv("GOZ_STORAGE_OPENSTACK_ACCESS")
	if gozOpenstackAccess != "" {
		cfg.Storage.Openstack.AccessKey = gozOpenstackAccess
	}
	gozOpenstackPassword := os.Getenv("GOZ_STORAGE_OPENSTACK_PASSWORD")
	if gozOpenstackPassword != "" {
		cfg.Storage.Openstack.AccessPassword = gozOpenstackPassword
	}
	gozOpenstackSecret := os.Getenv("GOZ_STORAGE_OPENSTACK_SECRET")
	if gozOpenstackSecret != "" {
		cfg.Storage.Openstack.Secret = gozOpenstackSecret
	}

	gozGpgKey := os.Getenv("GOZ_GPG_KEY")
	if gozGpgKey != "" {
		cfg.Gpg.Key = gozGpgKey
	}

	gozGpgPass := os.Getenv("GOZ_GPG_PASSPHRASE")
	if gozGpgPass != "" {
		cfg.Gpg.Passphrase = gozGpgPass
	}

	gozGpgPub := os.Getenv("GOZ_GPG_PUBKEY")
	if gozGpgPub != "" {
		cfg.Gpg.Pubkeyfile = gozGpgPub
	}

	gozInflux := os.Getenv("GOZ_INFLUXDB_HOST")
	if gozInflux != "" {
		cfg.Influxdb.Address = gozInflux
	}
	gozInfluxLogin := os.Getenv("GOZ_INFLUXDB_USER")
	if gozInfluxLogin != "" {
		cfg.Influxdb.Login = gozInfluxLogin
	}
	gozInfluxPassword := os.Getenv("GOZ_INFLUXDB_PASSWORD")
	if gozInfluxPassword != "" {
		cfg.Influxdb.Password = gozInfluxPassword
	}
	gozInfluxDb := os.Getenv("GOZ_INFLUXDB_DB")
	if gozInfluxDb != "" {
		cfg.Influxdb.Db = gozInfluxDb
	}
	jsonCfg, _ := json.Marshal(cfg)
	log.Debug().Str("cfg", string(jsonCfg)).Msg("config loaded")

	return cfg
}

// ConsulDeclare declare current service to consul
func ConsulDeclare(ctx GozContext, serviceName string) error {
	cfg := ctx.Config
	if cfg.Consul != "" {
		fmt.Printf("Declare service to consul at %s\n", cfg.Consul)
		consulCfg := consul.DefaultConfig()
		consulCfg.Address = cfg.Consul
		client, err := consul.NewClient(consulCfg)
		if err != nil {
			return err
		}
		hostname, _ := os.Hostname()
		tags := []string{
			"got",
			"api",
			"traefik.backend=" + serviceName,
			"traefik.frontend.rule=PathPrefix:/api,PathPrefix:/dl",
			"traefik.enable=true",
		}
		check := &consul.AgentServiceCheck{
			CheckID:  hostname,
			HTTP:     fmt.Sprintf("http://%s:%d%s", hostname, cfg.Web.Port, "/api"),
			Interval: "30s",
		}
		service := &consul.AgentServiceRegistration{
			ID:      hostname,
			Address: hostname,
			Name:    serviceName,
			Port:    int(cfg.Web.Port),
			Tags:    tags,
			Check:   check,
		}
		regerr := client.Agent().ServiceRegister(service)
		if regerr != nil {
			return regerr
		}
		fmt.Println("service register in consul to handle api calls to /api and /dl")
	}
	return nil

}

// CheckTokenString decodes fernet token
func CheckTokenString(goz GozContext, token string) (TokenMsg, error) {
	msg, errMsg := FernetDecode(goz.Config, []byte(token), true)

	var tokenMsg TokenMsg

	if errMsg != nil {
		return tokenMsg, errMsg
	}
	json.Unmarshal(msg, &tokenMsg)

	now := time.Now()
	ts := now.Unix()
	if tokenMsg.Expire > 0 && ts > tokenMsg.Expire {
		return tokenMsg, fmt.Errorf("token expired")
	}
	return tokenMsg, nil
}

// CheckTokenOrAnonymous checks token but can allow anonymous calls (no authorization header nor apikey headers)
func CheckTokenOrAnonymous(goz GozContext, headers http.Header, allowAnonymous bool) (user User, err error) {
	user, err = CheckToken(goz, headers)
	if allowAnonymous && user.LoginMode == LogAnonymous {
		return user, nil
	}
	return user, err
}

// CheckToken checks Fernet token
func CheckToken(goz GozContext, headers http.Header) (user User, err error) {
	authHeader := headers.Get("Authorization")
	if authHeader == "" {
		xUserHeader := headers.Get("X-GOZ-USER")
		xAPIKeyHeader := headers.Get("X-GOZ-APIKEY")
		if xUserHeader == "" || xAPIKeyHeader == "" {
			return User{
				LoginMode: LogAnonymous,
			}, fmt.Errorf("No auth information found in authorization nor X-GOZ headers")
		}
		user, userErr := UserAuth(goz, xUserHeader, xAPIKeyHeader)
		return user, userErr
	}
	authInfo := strings.Split(authHeader, " ")
	if len(authInfo) != 2 {
		return User{ID: ""}, fmt.Errorf("Invalid Authorization header")
	}
	bearer := strings.ToLower(authInfo[0])
	// Basic authentication
	if bearer == "basic" {
		creds := authInfo[1]
		b64Creds, b64Err := b64.StdEncoding.DecodeString(creds)
		if b64Err != nil {
			return user, fmt.Errorf("could not decode base64 authorization")
		}
		userCreds := strings.Split(string(b64Creds), ":")
		if len(userCreds) != 2 {
			return user, fmt.Errorf("could not decode base64 authorization")
		}
		// userid, apikey
		user, userErr := UserAuth(goz, userCreds[0], strings.TrimSuffix(userCreds[1], "\n"))
		if userErr != nil {
			// userid, password
			user, userErr = UserBind(goz, userCreds[0], strings.TrimSuffix(userCreds[1], "\n"))
		}
		return user, userErr
	}

	// Else bearer with Fernet token
	//tokenStr := strings.Replace(authHeader, "Bearer", "", -1)
	//tokenStr = strings.TrimSpace(tokenStr)
	tokenStr := authInfo[1]

	tokenMsg, errMsg := CheckTokenString(goz, tokenStr)
	if errMsg != nil {
		return user, errMsg
	}
	return tokenMsg.User, nil
}

func memberOf(user User, members []Member) (Profile, bool) {
	for _, member := range members {
		if member.User == user.ID {
			return member.Profile, true
		}
	}
	return ProfileAnonymous, false
}

// AmqpHandler manage amqp messages
type AmqpHandler struct {
	cfg          Config
	Conn         *amqp.Connection
	Ch           *amqp.Channel
	PublishQueue string
	DebianQueue  string
	RpmQueue     string
}

// NewAmqpHandler returns a new AmqpHandler instance
func NewAmqpHandler(cfg Config) (*AmqpHandler, error) {
	a := &AmqpHandler{
		cfg: cfg,
	}
	err := a.Handle()
	return a, err
}

// Handle initialize amqp echange and queue
func (a *AmqpHandler) Handle() error {
	if a.cfg.Amqp == "" {
		log.Error().Msg("no amqp defined")
		return fmt.Errorf("no amqp defined")
	}
	conn, err := amqp.Dial(a.cfg.Amqp)
	if err != nil {
		log.Error().Msgf("failed to connect to %s", a.cfg.Amqp)
		return err
	}
	a.Conn = conn

	ch, err := conn.Channel()
	if err != nil {
		log.Error().Msg("failed to connect to amqp")
		return err
	}

	err = ch.ExchangeDeclare(
		"goz",    // name
		"direct", // type
		true,     // durable
		false,    // auto-deleted
		false,    // internal
		false,    // no-wait
		nil,      // arguments
	)
	if err != nil {
		log.Error().Msg("failed to connect to open exchange")
		return err
	}

	queue, queueErr := ch.QueueDeclare(
		"gozpublish",
		true,  // durable
		false, // auto-deleted
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	a.PublishQueue = queue.Name

	if queueErr != nil {
		log.Error().Msg("failed to create queue")
		return queueErr
	}

	bindErr := ch.QueueBind(queue.Name, "publish", "goz", false, nil)
	if bindErr != nil {
		log.Error().Msg("failed to bind queue to exchange")
		return bindErr
	}

	debqueue, debqueueErr := ch.QueueDeclare(
		"gozdebian",
		true,  // durable
		false, // auto-deleted
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	a.DebianQueue = debqueue.Name

	if debqueueErr != nil {
		log.Error().Msg("failed to create queue")
		return queueErr
	}

	bindErr = ch.QueueBind(debqueue.Name, "debian", "goz", false, nil)
	if bindErr != nil {
		log.Error().Msg("failed to bind debian queue to exchange")
		return bindErr
	}

	rpmqueue, rpmqueueErr := ch.QueueDeclare(
		"gozrpm",
		true,  // durable
		false, // auto-deleted
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	a.RpmQueue = rpmqueue.Name

	if rpmqueueErr != nil {
		log.Error().Msg("failed to create queue")
		return queueErr
	}

	bindErr = ch.QueueBind(debqueue.Name, "rpm", "goz", false, nil)
	if bindErr != nil {
		log.Error().Msg("failed to bind rpm queue to exchange")
		return bindErr
	}

	a.Ch = ch
	return nil
}

// Random returns a random string of specified length
func Random(length int) string {
	rand.Seed(time.Now().UnixNano())
	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
		"0123456789")
	var b strings.Builder
	for i := 0; i < length; i++ {
		b.WriteRune(chars[rand.Intn(len(chars))])
	}
	return b.String()
}

// ConanInfo reads conaninfo file and returns struct mapping
func ConanInfo(goz GozContext, f FileObject) (map[string]interface{}, error) {
	// fetch file
	var iniStr []byte
	fh, fhErr := goz.Storage.GetObjectURL(goz, f, 60)
	if fhErr != nil {
		log.Error().Err(fhErr).Msg("failed to get conan info")
		return nil, fhErr
	}
	url := fmt.Sprintf("%s&signature=conan", fh)
	response, err := http.Get(url)
	if err != nil {
		log.Error().Err(err).Msg("failed to get conan info")
		return nil, err
	}
	defer response.Body.Close()
	iniStr, err = ioutil.ReadAll(response.Body)

	res := make(map[string]interface{})

	conanInfoFile := ini.Load(iniStr)
	sections := conanInfoFile.Sections()
	for _, section := range sections {

		if section.Name == "full_requires" {
			requires := make([]string, 0)
			keys := section.Keys()
			for _, key := range keys {
				requires = append(requires, key.Name())
			}
			res[section.Name] = requires
		} else {
			others := make(map[string]string)
			keys := section.Keys()
			for _, key := range keys {
				others[key.Name()] = section.GetValueWithDefault(key.Name(), "")
			}
			res[section.Name] = others
		}
	}
	return res, nil
}

func statAdd(goz GozContext, subject string, repo string, pack string, kind PackageType, stat string, fields map[string]interface{}) {
	config := goz.Config

	if config.Influxdb.Address == "" {
		return
	}

	log.Debug().Str("subject", subject).Str("repo", repo).Str("package", pack).Str("stat", stat).Msg("send stat")
	c, err := influx.NewHTTPClient(influx.HTTPConfig{
		Addr:     config.Influxdb.Address,
		Username: config.Influxdb.Login,
		Password: config.Influxdb.Password,
	})
	if err != nil {
		log.Error().Err(err).Msg("influxdb error")
		return
	}
	defer c.Close()

	bp, err := influx.NewBatchPoints(influx.BatchPointsConfig{
		Database:  config.Influxdb.Db,
		Precision: "s",
	})
	if err != nil {
		log.Error().Err(err).Msg("failed to create stat")
		return
	}

	skind := "raw"
	switch kind {
	case RawPackage:
		skind = "raw"
		break
	case DockerPackage:
		skind = "docker"
		break
	case MavenPackage:
		skind = "maven"
		break
	case DebianPackage:
		skind = "debian"
		break
	case RpmPackage:
		skind = "rpm"
		break
	case ConanPackage:
		skind = "conan"
		break
	default:
		break
	}

	subjectSearch := Subject{
		ID: subject,
	}
	subjectDB, subjectErr := subjectSearch.Exists(goz)
	if subjectErr != nil {
		log.Error().Err(subjectErr).Str("subject", subject).Msg("could not find subject")
		return
	}

	tags := map[string]string{"customer": subjectDB.Customer, "subject": subject, "repo": repo, "package": pack, "kind": skind}

	pt, err := influx.NewPoint(stat, tags, fields, time.Now())
	if err != nil {
		log.Error().Err(err).Msg("stat failure")
		return
	}
	bp.AddPoint(pt)

	// Write the batch
	if err := c.Write(bp); err != nil {
		log.Error().Err(err).Msg("stat sending failure")
		return
	}

	// Close client resources
	if err := c.Close(); err != nil {
		log.Error().Err(err).Msg("stat failed to close resource")
	}
}

// StatUpload add an upload stat for package
func StatUpload(goz GozContext, subject string, repo string, pack string, kind PackageType) {
	fields := map[string]interface{}{"value": 1}
	statAdd(goz, subject, repo, pack, kind, "goz.uploads", fields)
}

// StatDownload add a download stat for package
func StatDownload(goz GozContext, subject string, repo string, pack string, kind PackageType) {
	fields := map[string]interface{}{"value": 1}
	statAdd(goz, subject, repo, pack, kind, "goz.downloads", fields)
}

// StatQuota records for subject used size
func StatQuota(goz GozContext, subjectID string) {
	config := goz.Config

	if config.Influxdb.Address == "" {
		return
	}

	subjectSearch := Subject{
		ID: subjectID,
	}
	subject, subjectErr := subjectSearch.Exists(goz)
	if subjectErr != nil {
		log.Error().Err(subjectErr).Str("subject", subjectID).Msg("could not find subject")
		return
	}
	size, sizeErr := goz.Storage.GetQuota(goz, subjectID)
	if sizeErr != nil {
		log.Error().Err(sizeErr).Str("subject", subjectID).Msg("failed to get quota")
	}

	log.Debug().Str("subject", subjectID).Msg("send quota stat")
	c, err := influx.NewHTTPClient(influx.HTTPConfig{
		Addr:     config.Influxdb.Address,
		Username: config.Influxdb.Login,
		Password: config.Influxdb.Password,
	})
	if err != nil {
		log.Error().Err(err).Msg("influxdb error")
		return
	}
	defer c.Close()

	bp, err := influx.NewBatchPoints(influx.BatchPointsConfig{
		Database:  config.Influxdb.Db,
		Precision: "s",
	})
	if err != nil {
		log.Error().Err(err).Msg("failed to create stat")
		return
	}

	customerID := "none"
	if subject.Customer != "" {
		customerID = subject.Customer
	}
	tags := map[string]string{"subject": subjectID, " customer": customerID}
	fields := map[string]interface{}{"value": size}

	pt, err := influx.NewPoint("goz.quota", tags, fields, time.Now())
	if err != nil {
		log.Error().Err(err).Msg("stat failure")
		return
	}
	bp.AddPoint(pt)

	// Write the batch
	if err := c.Write(bp); err != nil {
		log.Error().Err(err).Msg("stat sending failure")
		return
	}

	// Close client resources
	if err := c.Close(); err != nil {
		log.Error().Err(err).Msg("stat failed to close resource")
	}
}

// GozEltType type of element
type GozEltType int

const (
	// GozEltSubject subject
	GozEltSubject GozEltType = 0
	// GozEltRepo repository
	GozEltRepo GozEltType = 1
	// GozEltPackage package
	GozEltPackage GozEltType = 2
	// GozEltPackageVersion package version
	GozEltPackageVersion GozEltType = 3
	// GozEltProduct product
	GozEltProduct GozEltType = 4
)

// GozElement store element for subject,repo,etc. in elasticsearch
type GozElement struct {
	Name        string     `json:"name"`
	Description string     `json:"description"`
	LastUpdated int64      `json:"last_updated"`
	Type        GozEltType `json:"type"`
	Subject     string     `json:"subject"`
	Repo        string     `json:"repo"`
	Package     string     `json:"package"`
	Version     string     `json:"version"`
}

// Search looks for search string in recorded elements
func (g GozElement) Search(goz GozContext, search string) ([]GozElement, error) {
	var buf bytes.Buffer

	query := map[string]interface{}{
		"query": map[string]interface{}{
			"bool": map[string]interface{}{
				"should": []map[string]interface{}{
					{"match": map[string]interface{}{
						"name": search,
					}},
					{"match": map[string]interface{}{
						"description": search,
					}},
				},
			},
		},
	}

	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		return nil, err
	}

	ctx := goz.Elastic.Search.WithContext(context.Background())
	// Perform the search request.
	res, err := goz.Elastic.Search(
		ctx,
		goz.Elastic.Search.WithIndex("gozelt"),
		goz.Elastic.Search.WithBody(&buf),
		goz.Elastic.Search.WithTrackTotalHits(true),
		goz.Elastic.Search.WithPretty(),
	)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			return nil, err
		}
		// Print the response status and error information.
		log.Error().Msgf("[%s] %s: %s",
			res.Status(),
			e["error"].(map[string]interface{})["type"],
			e["error"].(map[string]interface{})["reason"],
		)
		return nil, fmt.Errorf("%s", e["error"].(map[string]interface{})["reason"])

	}
	var searchRes map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&searchRes); err != nil {
		return nil, err
	}

	gelts := make([]GozElement, 0)

	for _, hit := range searchRes["hits"].(map[string]interface{})["hits"].([]interface{}) {
		source := hit.(map[string]interface{})["_source"].(map[string]interface{})

		ptype := GozEltType(source["type"].(float64))
		gelt := GozElement{
			Subject:     source["subject"].(string),
			Repo:        source["repo"].(string),
			Package:     source["package"].(string),
			Version:     source["version"].(string),
			Description: source["description"].(string),
			Type:        ptype,
		}

		gelts = append(gelts, gelt)
	}
	return gelts, nil
}

// Index add element to elasticsearch
func (g GozElement) Index(goz GozContext) error {
	fJSON, _ := json.Marshal(g)
	b64Id := b64.StdEncoding.EncodeToString([]byte(g.Name))
	req := esapi.IndexRequest{
		Index:      "gozelt",
		DocumentID: string(b64Id),
		Body:       strings.NewReader(string(fJSON)),
		Refresh:    "true",
	}

	res, err := req.Do(context.Background(), goz.Elastic)

	if err != nil {
		log.Error().Msgf("Error getting index response: %s", err)
		return err
	}
	defer res.Body.Close()
	if res.IsError() {
		log.Error().Msgf("index error %s", res.String())
		return fmt.Errorf("%s", res.String())
	}
	log.Debug().Str("element", g.Name).Msg("index")
	return nil
}

// UnIndex remove element from index
func (g GozElement) UnIndex(goz GozContext) error {
	b64Id := b64.StdEncoding.EncodeToString([]byte(g.Name))
	//log.Debug().Msgf("index %s", fmt.Sprintf("%s-%s-%s-%s-%s", f.Subject, f.Repo, f.Package, f.Version, f.Name))
	req := esapi.DeleteRequest{
		Index:      "gozelt",
		DocumentID: string(b64Id),
		Refresh:    "true",
	}

	res, err := req.Do(context.Background(), goz.Elastic)

	if err != nil {
		log.Error().Msgf("Error getting index response: %s", err)
		return err
	}
	defer res.Body.Close()
	if res.IsError() {
		log.Error().Msgf("index error %s", res.String())
		return fmt.Errorf("%s", res.String())
	}
	log.Debug().Str("element", g.Name).Msg("unindex")
	return nil
}

func customerStat(goz GozContext, customer string, stat string, fields map[string]interface{}) {
	config := goz.Config

	if config.Influxdb.Address == "" {
		return
	}

	log.Debug().Str("stat", stat).Msg("send customer stat")
	c, err := influx.NewHTTPClient(influx.HTTPConfig{
		Addr:     config.Influxdb.Address,
		Username: config.Influxdb.Login,
		Password: config.Influxdb.Password,
	})
	if err != nil {
		log.Error().Err(err).Msg("influxdb error")
		return
	}
	defer c.Close()

	bp, err := influx.NewBatchPoints(influx.BatchPointsConfig{
		Database:  config.Influxdb.Db,
		Precision: "s",
	})
	if err != nil {
		log.Error().Err(err).Msg("failed to create stat")
		return
	}

	tags := map[string]string{"customer": customer}

	pt, err := influx.NewPoint(stat, tags, fields, time.Now())
	if err != nil {
		log.Error().Err(err).Msg("stat failure")
		return
	}
	bp.AddPoint(pt)

	// Write the batch
	if err := c.Write(bp); err != nil {
		log.Error().Err(err).Msg("stat sending failure")
		return
	}

	// Close client resources
	if err := c.Close(); err != nil {
		log.Error().Err(err).Msg("stat failed to close resource")
	}
}

// CustomerStatSubjects update customer subjects stat
func CustomerStatSubjects(goz GozContext, customer string) {
	c := Customer{
		ID: customer,
	}
	nb, nbErr := c.GetNbSubjects(goz)
	if nbErr != nil {
		log.Error().Err(nbErr).Str("customer", customer).Msg("could not get customer subject count")
		return
	}
	fields := map[string]interface{}{"value": nb}
	customerStat(goz, customer, "goz.customer.subject", fields)
}
