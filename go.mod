module gitlab.inria.fr/osallou/gozilla-lib

go 1.13

require (
	github.com/DataDog/zstd v1.4.4 // indirect
	github.com/dustin/go-humanize v1.0.0
	github.com/elastic/go-elasticsearch v0.0.0
	github.com/fernet/fernet-go v0.0.0-20191111064656-eff2850e6001
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/go-cmp v0.4.0 // indirect
	github.com/gophercloud/gophercloud v0.7.0
	github.com/hashicorp/consul/api v1.3.0
	github.com/influxdata/influxdb-client-go v0.1.5
	github.com/influxdata/influxdb1-client v0.0.0-20191209144304-8bf82d3c094d
	github.com/kr/pretty v0.2.0 // indirect
	github.com/ochinchina/go-ini v1.0.1
	github.com/rs/zerolog v1.17.2
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	github.com/tidwall/pretty v1.0.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.2.1
	golang.org/x/crypto v0.0.0-20191202143827-86a70503ff7e
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/yaml.v2 v2.2.7
)
