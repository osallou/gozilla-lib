package gozillalib

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/rs/zerolog/log"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"gopkg.in/mgo.v2/bson"
)

// Profile type
type Profile int

const (
	// ProfileUnknown not used
	ProfileUnknown Profile = -1
	// ProfileAnonymous describe an anonymous user (no access)
	ProfileAnonymous Profile = 0
	// ProfileAdmin describe a gozilla admin (*:rw, *:admin)
	ProfileAdmin Profile = 1
	// ProfileRepoAdmin describe a repository admin (repo:rw, repo:admin)
	ProfileRepoAdmin Profile = 10
	// ProfileReleaseManager (repo:rw, repo:release:rw)
	ProfileReleaseManager Profile = 20
	// ProfileDev describe a repository developper (repo:rw)
	ProfileDev Profile = 30
	// ProfileGuest describe a repository guest (repo:r)
	ProfileGuest Profile = 40
)

// String gets textual representation of profile
func (p Profile) String() string {
	switch p {
	case -1:
		return "unknown"
	case 0:
		return "anonymous"
	case 1:
		return "admin"
	case 10:
		return "repository admin"
	case 20:
		return "release manager"
	case 30:
		return "developper"
	case 40:
		return "guest"
	default:
		return "unknown"
	}

}

// CustomProfile TODO (not yet used)
type CustomProfile struct {
	CanReadSubject   bool
	CanWriteSubject  bool
	CanDeleteSubject bool
	CanReadRepo      bool
	CanWriteRepo     bool
	CanDeleteRepo    bool
	CanWritePackage  bool
	CanDeletePackage bool
}

// Member is a repository member
type Member struct {
	User    string  `json:"user"` // user id
	Profile Profile `json:"profile"`
}

// SubjectType defines the kind of subject
type SubjectType int

const (
	// SubjectPersonal is used for users
	SubjectPersonal SubjectType = 0
	// SubjectOrg is used for organizations
	SubjectOrg SubjectType = 1
)

// VisibilityType defines the visibility levels
type VisibilityType int

const (
	// VisibilityPublic visible for anyone
	VisibilityPublic VisibilityType = 0
	// VisibilityProtected accessible to any logged user
	VisibilityProtected VisibilityType = 1
	// VisibilityPrivate visible only by subject/repo members
	VisibilityPrivate VisibilityType = 2
)

// Subject is a user or an organization
type Subject struct {
	ID          string         `json:"id"`
	Type        SubjectType    `json:"type"`
	Owner       string         `json:"owner"`    // user id
	Members     []Member       `json:"members"`  // members of organization
	Customer    string         `json:"customer"` // customer id
	Visibility  VisibilityType `json:"visibility"`
	Description string         `json:"description"`
	Created     int64          `json:"created"`
	LastUpdated int64          `json:"last_updated"`
}

// CanRead TODO
func (s *Subject) CanRead(goz GozContext) bool {
	switch s.Visibility {
	case VisibilityPublic:
		return true
	case VisibilityProtected:
		if goz.User.ID != "" {
			return true
		}
		return false
	default:
		if goz.User.ID == "" {
			return false
		}
		_, isMember := s.MemberOf(goz, goz.User)
		return isMember
	}
}

// CanRelease determines if user can create releases for this subject
func (s *Subject) CanRelease(goz GozContext) bool {
	if !goz.User.Active() {
		return false
	}
	if goz.User.ID == "" {
		return false
	}
	profile, isMember := s.MemberOf(goz, goz.User)
	if isMember && profile >= ProfileAdmin && profile <= ProfileReleaseManager {
		return true
	}
	return false
}

// CanWrite TODO
func (s *Subject) CanWrite(goz GozContext) bool {
	if !goz.User.Active() {
		return false
	}
	if goz.User.ID == "" {
		return false
	}
	profile, isMember := s.MemberOf(goz, goz.User)
	if isMember && profile >= ProfileAdmin && profile < ProfileGuest {
		return true
	}
	return false
}

// Exists checks if subject exists
func (s *Subject) Exists(goz GozContext) (Subject, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	subjectCollection := mongoClient.Database(config.Mongo.DB).Collection(SubjectColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"id": s.ID,
	}
	var subject Subject
	subjectErr := subjectCollection.FindOne(ctx, filter).Decode(&subject)
	return subject, subjectErr
}

// MemberOf determines if user part of members
func (s *Subject) MemberOf(goz GozContext, user User) (Profile, bool) {
	if !goz.User.Active() {
		return ProfileAnonymous, false
	}
	if user.IsAdmin() {
		return ProfileAdmin, true
	}
	if s.Owner == user.ID {
		return ProfileRepoAdmin, true
	}
	profile, isMember := memberOf(user, s.Members)
	return profile, isMember
}

// List returns subjects user is owner or member of
func (s *Subject) List(goz GozContext) ([]Subject, error) {
	mongoClient := goz.Mongo
	subjects := make([]Subject, 0)
	config := goz.Config
	conditions := make([]bson.M, 2)
	conditions[0] = bson.M{
		"owner": goz.User.ID,
	}
	conditions[1] = bson.M{
		"members": goz.User.ID,
	}
	subjectSearch := bson.M{
		"$or": conditions,
	}
	subjectCollection := mongoClient.Database(config.Mongo.DB).Collection(SubjectColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	cursor, err := subjectCollection.Find(ctx, subjectSearch)
	if err != nil {
		return subjects, err
	}
	for cursor.Next(ctx) {
		var subject Subject
		cursor.Decode(&subject)
		subjects = append(subjects, subject)
	}
	return subjects, err
}

// GetRepo gets repository info in *subject*
func (s *Subject) GetRepo(goz GozContext, repoID string) (Repo, error) {
	repoSearch := Repo{
		ID:      repoID,
		Subject: s.ID,
	}
	repo, repoErr := repoSearch.Get(goz)
	return repo, repoErr
}

// ListRepo list repositories info in *subject*
func (s *Subject) ListRepo(goz GozContext) ([]Repo, error) {
	repoSearch := Repo{
		Subject: s.ID,
	}
	repo, repoErr := repoSearch.List(goz)
	return repo, repoErr
}

// Products list products in *subject*
func (s *Subject) Products(goz GozContext) ([]Product, error) {
	pSearch := Product{
		Subject: s.ID,
	}
	products, productsErr := pSearch.List(goz)
	return products, productsErr
}

// Delete removes subject from db
func (s *Subject) Delete(goz GozContext) error {
	mongoClient := goz.Mongo
	config := goz.Config
	subjectCollection := mongoClient.Database(config.Mongo.DB).Collection(SubjectColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := subjectCollection.DeleteOne(ctx, bson.M{
		"id": s.ID,
	})
	GozElement{
		Name: fmt.Sprintf("%s", s.ID),
	}.UnIndex(goz)
	if s.Customer != "" {
		CustomerStatSubjects(goz, s.Customer)
	}
	return err
}

// Create a user or org root
func (s *Subject) Create(goz GozContext) error {
	if _, err := s.Exists(goz); err == nil {
		return fmt.Errorf("subject already exists")
	}

	plan, planErr := goz.User.GetPlan(goz)
	if planErr != nil {
		return planErr
	}
	if plan.Kind == PlanGuest {
		return fmt.Errorf("using guest plan, subject creation not allowed")
	}

	// for organizations
	if s.Type == SubjectOrg {
		if plan.Quota.Subjects == 0 && !goz.User.IsOperator() {
			return fmt.Errorf("only operators can create organisations")
		}
		if plan.Quota.Subjects > 0 {
			subjects, sErr := goz.User.OwnedSubjects(goz)
			if sErr != nil {
				log.Error().Err(sErr).Str("user", goz.User.ID).Msg("subject creation failed")
				return sErr
			}
			if len(subjects) > plan.Quota.Subjects {
				log.Info().Str("user", goz.User.ID).Msg("subjects quota reached")
				return fmt.Errorf("max subjects quota reached")
			}
		}
	}

	now := time.Now()
	ts := now.Unix()
	s.Created = ts
	s.LastUpdated = ts
	mongoClient := goz.Mongo
	config := goz.Config
	subjectCollection := mongoClient.Database(config.Mongo.DB).Collection(SubjectColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := subjectCollection.InsertOne(ctx, s)
	if err != nil {
		return err
	}
	err = goz.Storage.CreateBucket(goz, s.ID)

	GozElement{
		Name:        s.ID,
		LastUpdated: s.LastUpdated,
		Description: s.Description,
		Type:        GozEltSubject,
		Subject:     s.ID,
	}.Index(goz)

	if s.Customer != "" {
		CustomerStatSubjects(goz, s.Customer)
	}

	return err
}

// Save updates subject info
func (s *Subject) Save(goz GozContext) error {
	now := time.Now()
	ts := now.Unix()
	s.LastUpdated = ts
	mongoClient := goz.Mongo
	config := goz.Config
	subjectCollection := mongoClient.Database(config.Mongo.DB).Collection(SubjectColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"id": s.ID,
	}
	_, err := subjectCollection.ReplaceOne(ctx, filter, s)

	GozElement{
		Name:        s.ID,
		LastUpdated: s.LastUpdated,
		Description: s.Description,
		Type:        GozEltSubject,
		Subject:     s.ID,
	}.Index(goz)
	return err
}

// Save updates repo info
func (r *Repo) Save(goz GozContext) error {
	now := time.Now()
	ts := now.Unix()
	r.LastUpdated = ts
	mongoClient := goz.Mongo
	config := goz.Config
	repoCollection := mongoClient.Database(config.Mongo.DB).Collection(RepoColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"id":      r.ID,
		"subject": r.Subject,
	}
	_, err := repoCollection.ReplaceOne(ctx, filter, r)

	GozElement{
		Name:        fmt.Sprintf("%s %s", r.Subject, r.ID),
		LastUpdated: r.LastUpdated,
		Description: r.Description,
		Type:        GozEltRepo,
		Subject:     r.Subject,
		Repo:        r.ID,
	}.Index(goz)
	return err
}

// Repo is a repository
type Repo struct {
	ID          string            `json:"id"`
	Owner       string            `json:"owner"`
	Description string            `json:"description"`
	Members     []Member          `json:"members"`
	Subject     string            `json:"subject"` // subject id
	Visibility  VisibilityType    `json:"visibility"`
	Meta        map[string]string `json:"meta"` // Metadata package dependant
	Created     int64             `json:"created"`
	LastUpdated int64             `json:"last_updated"`
	HomePage    string            `json:"homepage"`
	IssuesPage  string            `json:"issues"`
	CodePage    string            `json:"code"`
}

// CanRead TODO
func (r *Repo) CanRead(goz GozContext) bool {
	if !goz.User.Active() {
		return false
	}
	switch r.Visibility {
	case VisibilityPublic:
		return true
	case VisibilityProtected:
		if goz.User.ID != "" {
			return true
		}
		return false
	default:
		if goz.User.ID == "" {
			return false
		}
		_, isMember := r.MemberOf(goz, goz.User)
		return isMember
	}
}

// CanWrite TODO
func (r *Repo) CanWrite(goz GozContext) bool {
	if !goz.User.Active() {
		return false
	}

	if goz.User.ID == "" {
		return false
	}
	profile, isMember := r.MemberOf(goz, goz.User)
	if isMember && profile >= ProfileAdmin && profile < ProfileGuest {
		return true
	}
	return false
}

// Create a repository
func (r *Repo) Create(goz GozContext) error {
	if _, err := r.Exists(goz); err == nil {
		return fmt.Errorf("repo already exists")
	}

	now := time.Now()
	ts := now.Unix()
	if r.Owner == "" {
		r.Owner = goz.User.ID
	}
	r.Created = ts
	r.LastUpdated = ts
	mongoClient := goz.Mongo
	config := goz.Config
	repoCollection := mongoClient.Database(config.Mongo.DB).Collection(RepoColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := repoCollection.InsertOne(ctx, r)
	if err != nil {
		return err
	}
	err = goz.Storage.CreateRepo(goz, r.Subject, r.ID)

	GozElement{
		Name:        fmt.Sprintf("%s %s", r.Subject, r.ID),
		LastUpdated: r.LastUpdated,
		Description: r.Description,
		Type:        GozEltRepo,
		Subject:     r.Subject,
		Repo:        r.ID,
	}.Index(goz)
	return err
}

// Delete removes subject from db
func (r *Repo) Delete(goz GozContext) error {
	mongoClient := goz.Mongo
	config := goz.Config
	repoCollection := mongoClient.Database(config.Mongo.DB).Collection(RepoColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := repoCollection.DeleteOne(ctx, bson.M{
		"id":      r.ID,
		"subject": r.Subject,
	})
	GozElement{
		Name: fmt.Sprintf("%s %s", r.Subject, r.ID),
	}.UnIndex(goz)
	return err
}

// Exists checks if subjecrepot exists
func (r *Repo) Exists(goz GozContext) (Repo, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	repoCollection := mongoClient.Database(config.Mongo.DB).Collection(RepoColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"id":      r.ID,
		"subject": r.Subject,
	}
	var repo Repo
	repoErr := repoCollection.FindOne(ctx, filter).Decode(&repo)
	return repo, repoErr
}

// Get returns repository information
func (r *Repo) Get(goz GozContext) (Repo, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	repoCollection := mongoClient.Database(config.Mongo.DB).Collection(RepoColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	filter := bson.M{
		"subject": r.Subject,
		"id":      r.ID,
	}
	var repo Repo
	repoErr := repoCollection.FindOne(ctx, filter).Decode(&repo)
	return repo, repoErr
}

// List returns repositories information
func (r *Repo) List(goz GozContext) ([]Repo, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	repoCollection := mongoClient.Database(config.Mongo.DB).Collection(RepoColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	filter := bson.M{
		"subject": r.Subject,
	}
	repos := make([]Repo, 0)

	cursor, cursorErr := repoCollection.Find(ctx, filter)
	if cursorErr == nil {
		for cursor.Next(ctx) {
			var repo Repo
			cursor.Decode(&repo)
			repos = append(repos, repo)
		}
	}
	return repos, cursorErr
}

// ListPackages TODO
func (r *Repo) ListPackages(goz GozContext) ([]Package, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	packCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	filter := bson.M{
		"subject": r.Subject,
		"repo":    r.ID,
	}
	packages := make([]Package, 0)

	cursor, cursorErr := packCollection.Find(ctx, filter)
	if cursorErr == nil {
		for cursor.Next(ctx) {
			var pack Package
			cursor.Decode(&pack)
			packages = append(packages, pack)
		}
	}
	return packages, cursorErr
}

// MemberOf checks if user member of repo or subject
func (r *Repo) MemberOf(goz GozContext, user User) (Profile, bool) {
	if !goz.User.Active() {
		return ProfileAnonymous, false
	}

	if user.IsAdmin() {
		return ProfileAdmin, true
	}

	if r.Owner == user.ID {
		return ProfileRepoAdmin, true
	}

	profile, isMember := memberOf(user, r.Members)
	if !isMember {
		subjectRepo := Subject{ID: r.Subject}
		subject, _ := subjectRepo.Exists(goz)
		profile, isMember = subject.MemberOf(goz, user)
	}
	return profile, isMember
}

// Files returns list of files for a repo
func (r Repo) Files(goz GozContext) ([]FileObject, error) {
	var buf bytes.Buffer

	query := map[string]interface{}{
		"query": map[string]interface{}{
			"match": map[string]interface{}{
				"irepo": fmt.Sprintf("%s_%s", r.Subject, r.ID),
			},
		},
	}

	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		return nil, err
	}

	ctx := goz.Elastic.Search.WithContext(context.Background())
	// Perform the search request.
	res, err := goz.Elastic.Search(
		ctx,
		goz.Elastic.Search.WithIndex("gozfile"),
		goz.Elastic.Search.WithBody(&buf),
		goz.Elastic.Search.WithTrackTotalHits(true),
		goz.Elastic.Search.WithPretty(),
	)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			return nil, err
		}
		// Print the response status and error information.
		log.Error().Msgf("[%s] %s: %s",
			res.Status(),
			e["error"].(map[string]interface{})["type"],
			e["error"].(map[string]interface{})["reason"],
		)
		return nil, fmt.Errorf("%s", e["error"].(map[string]interface{})["reason"])

	}
	var searchRes map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&searchRes); err != nil {
		return nil, err
	}
	fos := make([]FileObject, 0)

	for _, hit := range searchRes["hits"].(map[string]interface{})["hits"].([]interface{}) {
		source := hit.(map[string]interface{})["_source"].(map[string]interface{})

		ptype := PackageType(source["PackageType"].(float64))
		var fo FileObject
		switch ptype {
		case RawPackage:
			fo = NewRawFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case MavenPackage:
			fo = NewMavenFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case DebianPackage:
			fo = NewDebianFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case RpmPackage:
			fo = NewRpmFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case ConanPackage:
			fo = NewConanFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		default:
			fo = NewRawFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		}
		//fo.SetType(ptype)

		fos = append(fos, fo)
	}

	return fos, nil
}

// PackageType defines the kind of package
type PackageType int

const (
	// RawPackage manages raw files
	RawPackage PackageType = 0
	// MavenPackage manages Maven repo
	MavenPackage PackageType = 1
	// DebianPackage manages Debian repo
	DebianPackage PackageType = 2
	// RpmPackage manages RPM repo
	RpmPackage PackageType = 3
	// DockerPackage manages Docker repo
	DockerPackage PackageType = 4
	// ConanPackage manages Conan repo
	ConanPackage PackageType = 5
)

// Package represents a package
type Package struct {
	ID          string            `json:"id"`
	Subject     string            `json:"subject"`
	Repo        string            `json:"repo"`
	Created     int64             `json:"created"`
	LastUpdated int64             `json:"last_updated"`
	Deleted     bool              `json:"deleted"`
	DeletedTime int64             `json:"deleted_ts"`
	Description string            `json:"description"`
	Type        PackageType       `json:"type"`
	Meta        map[string]string `json:"meta"` // Metadata package dependant
	Versions    []string          `json:"versions"`
}

// PackageVersion represents a package version
type PackageVersion struct {
	Version       string            `json:"version"`
	Subject       string            `json:"subject"`
	Repo          string            `json:"repo"`
	Package       string            `json:"package"`
	Created       int64             `json:"created"`
	LastUpdated   int64             `json:"last_updated"`
	Published     bool              `json:"published"`
	PublishedTime int64             `json:"published_ts"`
	ToPublish     bool              `json:"to_publish"` // Should package be published
	Deleted       bool              `json:"deleted"`
	DeletedTime   int64             `json:"deleted_ts"`
	Meta          map[string]string `json:"meta"` // Metadata package dependant
	Description   string            `json:"description"`
}

// Files returns list of files for a package version
func (p PackageVersion) Files(goz GozContext) ([]FileObject, error) {
	var buf bytes.Buffer

	query := map[string]interface{}{
		"query": map[string]interface{}{
			"match": map[string]interface{}{
				"ivpack": fmt.Sprintf("%s_%s_%s_%s", p.Subject, p.Repo, p.Package, p.Version),
			},
		},
	}

	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		return nil, err
	}

	ctx := goz.Elastic.Search.WithContext(context.Background())
	// Perform the search request.
	res, err := goz.Elastic.Search(
		ctx,
		goz.Elastic.Search.WithIndex("gozfile"),
		goz.Elastic.Search.WithBody(&buf),
		goz.Elastic.Search.WithTrackTotalHits(true),
		goz.Elastic.Search.WithPretty(),
	)
	if err != nil {
		log.Error().Msgf("search error %s", err)
		return nil, err
	}
	defer res.Body.Close()
	if res.IsError() {
		log.Error().Msgf("index error %s", res.String())
		return nil, fmt.Errorf("%s", res.String())

	}
	var r map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		return nil, err
	}
	fos := make([]FileObject, 0)

	for _, hit := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {
		source := hit.(map[string]interface{})["_source"].(map[string]interface{})
		ptype := PackageType(source["PackageType"].(float64))
		var fo FileObject
		switch ptype {
		case RawPackage:
			fo = NewRawFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case MavenPackage:
			fo = NewMavenFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case DebianPackage:
			fo = NewDebianFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case RpmPackage:
			fo = NewRpmFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case ConanPackage:
			fo = NewConanFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		default:
			fo = NewRawFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		}
		extras := make(map[string]interface{})
		if source["Extras"] != nil {
			extras = source["Extras"].(map[string]interface{})
		}
		metas := source["Meta"].(map[string]interface{})
		fi := FileInfo{
			LastUpdated: int64(metas["last_updated"].(float64)),
			Size:        int64(metas["size"].(float64)),
			MD5:         metas["md5"].(string),
		}
		fo.SetExtras(extras)
		fo.SetMeta(fi)
		fo.SetType(ptype)

		fos = append(fos, fo)
	}

	return fos, nil
}

// Files returns list of files for a package
func (p Package) Files(goz GozContext) ([]FileObject, error) {
	var buf bytes.Buffer

	query := map[string]interface{}{
		"query": map[string]interface{}{
			"match": map[string]interface{}{
				"ipack": fmt.Sprintf("%s_%s_%s", p.Subject, p.Repo, p.ID),
			},
		},
	}

	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		return nil, err
	}

	ctx := goz.Elastic.Search.WithContext(context.Background())
	// Perform the search request.
	res, err := goz.Elastic.Search(
		ctx,
		goz.Elastic.Search.WithIndex("gozfile"),
		goz.Elastic.Search.WithBody(&buf),
		goz.Elastic.Search.WithTrackTotalHits(true),
		goz.Elastic.Search.WithPretty(),
	)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			return nil, err
		}
		// Print the response status and error information.
		log.Error().Msgf("[%s] %s: %s",
			res.Status(),
			e["error"].(map[string]interface{})["type"],
			e["error"].(map[string]interface{})["reason"],
		)
		return nil, fmt.Errorf("%s", e["error"].(map[string]interface{})["reason"])

	}
	var r map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		return nil, err
	}
	fos := make([]FileObject, 0)

	for _, hit := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {
		source := hit.(map[string]interface{})["_source"].(map[string]interface{})

		ptype := PackageType(source["PackageType"].(float64))
		var fo FileObject
		switch ptype {
		case RawPackage:
			fo = NewRawFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case MavenPackage:
			fo = NewMavenFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case DebianPackage:
			fo = NewDebianFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case RpmPackage:
			fo = NewRpmFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case ConanPackage:
			fo = NewConanFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		default:
			fo = NewRawFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		}
		//fo.SetType(ptype)

		fos = append(fos, fo)
	}

	return fos, nil
}

// SetMetaFiles find files and update md5 etc...
func (p PackageVersion) SetMetaFiles(goz GozContext) ([]FileObject, error) {
	var buf bytes.Buffer

	queryMatches := make([]map[string]map[string]string, 4)
	queryMatches[0] = make(map[string]map[string]string)
	queryMatches[0]["match"] = make(map[string]string)
	queryMatches[0]["match"]["Subject"] = p.Subject

	queryMatches[1] = make(map[string]map[string]string)
	queryMatches[1]["match"] = make(map[string]string)
	queryMatches[1]["match"]["Repo"] = p.Repo

	queryMatches[2] = make(map[string]map[string]string)
	queryMatches[2]["match"] = make(map[string]string)
	queryMatches[2]["match"]["Package"] = p.Package

	queryMatches[3] = make(map[string]map[string]string)
	queryMatches[3]["match"] = make(map[string]string)
	queryMatches[3]["match"]["Version"] = p.Version

	query := map[string]interface{}{
		"query": map[string]interface{}{
			"bool": map[string]interface{}{
				"must": queryMatches,
			},
		},
	}

	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		return nil, err
	}

	ctx := goz.Elastic.Search.WithContext(context.Background())
	// Perform the search request.
	res, err := goz.Elastic.Search(
		ctx,
		goz.Elastic.Search.WithIndex("gozfile"),
		goz.Elastic.Search.WithBody(&buf),
		goz.Elastic.Search.WithTrackTotalHits(true),
		goz.Elastic.Search.WithPretty(),
	)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			return nil, err
		}
		// Print the response status and error information.
		log.Error().Msgf("[%s] %s: %s",
			res.Status(),
			e["error"].(map[string]interface{})["type"],
			e["error"].(map[string]interface{})["reason"],
		)
		return nil, fmt.Errorf("%s", e["error"].(map[string]interface{})["reason"])

	}
	var r map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		return nil, err
	}
	fos := make([]FileObject, 0)

	for _, hit := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {
		source := hit.(map[string]interface{})["_source"].(map[string]interface{})
		ptype := PackageType(source["PackageType"].(float64))
		var fo FileObject
		switch ptype {
		case RawPackage:
			fo = NewRawFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case MavenPackage:
			fo = NewMavenFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case DebianPackage:
			fo = NewDebianFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case RpmPackage:
			fo = NewRpmFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		case ConanPackage:
			fo = NewConanFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		default:
			fo = NewRawFileObject(
				source["Subject"].(string),
				source["Repo"].(string),
				source["Package"].(string),
				source["Version"].(string),
				source["Name"].(string),
			)
		}
		//fo.SetType(ptype)

		fos = append(fos, fo)
	}
	for _, fo := range fos {
		fileInfo, fileErr := goz.Storage.MD5(goz, fo)
		if fileErr != nil {
			log.Error().Str("file", fo.GetPath()).Msg("should remove from index")
		}
		fo.SetMeta(fileInfo)
		fo.Index(goz)
	}
	return fos, nil
}

// Exists checks if subject exists
func (p *Package) Exists(goz GozContext) (Package, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	packCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"id":      p.ID,
		"subject": p.Subject,
		"repo":    p.Repo,
	}
	var pack Package
	packErr := packCollection.FindOne(ctx, filter).Decode(&pack)
	return pack, packErr
}

// GetVersions get package available versions
func (p *Package) GetVersions(goz GozContext) ([]PackageVersion, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	vpackCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageVersionColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"package": p.ID,
		"subject": p.Subject,
		"repo":    p.Repo,
	}
	versions := make([]PackageVersion, 0)
	cursor, cursorErr := vpackCollection.Find(ctx, filter)
	if cursorErr == nil {
		for cursor.Next(ctx) {
			var vpack PackageVersion
			cursor.Decode(&vpack)
			versions = append(versions, vpack)
		}
	}
	return versions, cursorErr
}

// Exists checks if subject exists
func (p *PackageVersion) Exists(goz GozContext) (PackageVersion, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	vpackCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageVersionColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"package": p.Package,
		"version": p.Version,
		"subject": p.Subject,
		"repo":    p.Repo,
	}
	var pack PackageVersion
	packErr := vpackCollection.FindOne(ctx, filter).Decode(&pack)
	return pack, packErr
}

// Create adds a new package
func (p *Package) Create(goz GozContext) error {
	log.Debug().Str("pack", p.ID).Msg("create package")
	now := time.Now()
	ts := now.Unix()
	p.Created = ts
	p.LastUpdated = ts
	mongoClient := goz.Mongo
	config := goz.Config
	packCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := packCollection.InsertOne(ctx, p)

	GozElement{
		Name:        fmt.Sprintf("%s %s %s", p.Subject, p.Repo, p.ID),
		LastUpdated: p.LastUpdated,
		Description: p.Description,
		Type:        GozEltPackage,
		Subject:     p.Subject,
		Repo:        p.Repo,
		Package:     p.ID,
	}.Index(goz)
	return err
}

// Delete removes package from db
func (p *Package) Delete(goz GozContext) error {
	mongoClient := goz.Mongo
	config := goz.Config
	packCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := packCollection.DeleteOne(ctx, bson.M{
		"id":      p.ID,
		"subject": p.Subject,
		"repo":    p.Repo,
	})
	GozElement{
		Name: fmt.Sprintf("%s %s %s", p.Subject, p.Repo, p.ID),
	}.UnIndex(goz)
	return err
}

// Create adds a new package version
func (p *PackageVersion) Create(goz GozContext) error {
	log.Debug().Str("pack", p.Package).Str("version", p.Version).Msg("create package version")
	now := time.Now()
	ts := now.Unix()
	p.Created = ts
	p.LastUpdated = ts
	mongoClient := goz.Mongo
	config := goz.Config
	vpackCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageVersionColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := vpackCollection.InsertOne(ctx, p)
	GozElement{
		Name:        fmt.Sprintf("%s %s %s %s", p.Subject, p.Repo, p.Package, p.Version),
		LastUpdated: p.LastUpdated,
		Description: p.Description,
		Type:        GozEltPackageVersion,
		Subject:     p.Subject,
		Repo:        p.Repo,
		Package:     p.Package,
		Version:     p.Version,
	}.Index(goz)
	return err
}

// Delete removes package version from db
func (p *PackageVersion) Delete(goz GozContext) error {
	mongoClient := goz.Mongo
	config := goz.Config
	packCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageVersionColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := packCollection.DeleteOne(ctx, bson.M{
		"version": p.Version,
		"subject": p.Subject,
		"repo":    p.Repo,
		"package": p.Package,
	})
	GozElement{
		Name: fmt.Sprintf("%s %s %s %s", p.Subject, p.Repo, p.Package, p.Version),
	}.UnIndex(goz)
	return err
}

// Save update an existing package
func (p *Package) Save(goz GozContext) error {
	now := time.Now()
	ts := now.Unix()
	p.LastUpdated = ts
	log.Debug().Str("pack", p.ID).Msg("update package")
	mongoClient := goz.Mongo
	config := goz.Config
	packCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"id":      p.ID,
		"subject": p.Subject,
		"repo":    p.Repo,
	}
	_, err := packCollection.ReplaceOne(ctx, filter, p)
	GozElement{
		Name:        fmt.Sprintf("%s %s %s", p.Subject, p.Repo, p.ID),
		LastUpdated: p.LastUpdated,
		Description: p.Description,
		Type:        GozEltPackage,
		Subject:     p.Subject,
		Repo:        p.Repo,
		Package:     p.ID,
	}.Index(goz)
	return err
}

// Save update an existing package
func (p *PackageVersion) Save(goz GozContext) error {
	now := time.Now()
	ts := now.Unix()
	p.LastUpdated = ts
	log.Debug().Str("pack", p.Package).Str("version", p.Version).Msg("update package")
	mongoClient := goz.Mongo
	config := goz.Config
	vpackCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageVersionColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"package": p.Package,
		"version": p.Version,
		"subject": p.Subject,
		"repo":    p.Repo,
	}
	_, err := vpackCollection.ReplaceOne(ctx, filter, p)
	GozElement{
		Name:        fmt.Sprintf("%s %s %s %s", p.Subject, p.Repo, p.Package, p.Version),
		LastUpdated: p.LastUpdated,
		Description: p.Description,
		Type:        GozEltPackageVersion,
		Subject:     p.Subject,
		Repo:        p.Repo,
		Package:     p.Package,
		Version:     p.Version,
	}.Index(goz)
	return err
}

// Latest returns latest version (last updated package)
func (p *PackageVersion) Latest(goz GozContext) (string, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	vpackageCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageVersionColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	filter := bson.M{
		"subject": p.Subject,
		"repo":    p.Repo,
		"package": p.Package,
	}

	latest := ""
	cursor, cursorErr := vpackageCollection.Find(ctx, filter)
	if cursorErr == nil {
		lastUpdate := PackageVersion{}
		for cursor.Next(ctx) {
			var vpack PackageVersion
			cursor.Decode(&vpack)
			if vpack.LastUpdated > lastUpdate.LastUpdated {
				lastUpdate = vpack
			}
		}
		latest = lastUpdate.Version
	} else {
		return "", cursorErr
	}
	return latest, nil
}

// Get returns package information with all versions
func (p *Package) Get(goz GozContext) (Package, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	vpackCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageVersionColl)
	packCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageColl)

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	packfilter := bson.M{
		"subject": p.Subject,
		"repo":    p.Repo,
		"id":      p.ID,
	}
	var pack Package
	packErr := packCollection.FindOne(ctx, packfilter).Decode(&pack)

	if packErr != nil {
		return pack, packErr
	}

	filter := bson.M{
		"subject": p.Subject,
		"repo":    p.Repo,
		"package": p.ID,
	}

	versions := make([]string, 0)
	cursor, cursorErr := vpackCollection.Find(ctx, filter)

	if cursorErr == nil {
		for cursor.Next(ctx) {
			var vpack PackageVersion
			cursor.Decode(&vpack)
			versions = append(versions, vpack.Version)
		}
	}
	pack.Versions = versions
	return pack, nil
}

// Get returns package version information
func (p *PackageVersion) Get(goz GozContext) (PackageVersion, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	vpackCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageVersionColl)

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	filter := bson.M{
		"subject": p.Subject,
		"repo":    p.Repo,
		"package": p.Package,
		"version": p.Version,
	}
	var pack PackageVersion
	packErr := vpackCollection.FindOne(ctx, filter).Decode(&pack)

	return pack, packErr
}

// Product represents a product ie a combination of package
type Product struct {
	ID          string            `json:"id"`
	Description string            `json:"description"`
	Subject     string            `json:"subject"`
	Created     int64             `json:"created"`
	LastUpdated int64             `json:"last_updated"`
	Deleted     bool              `json:"deleted"`
	DeletedTime int64             `json:"deleted_ts"`
	Meta        map[string]string `json:"meta"` // Metadata
	Versions    []string          `json:"versions"`
}

// Create a product
func (p *Product) Create(goz GozContext) error {
	now := time.Now()
	ts := now.Unix()
	p.Created = ts
	mongoClient := goz.Mongo
	config := goz.Config
	productCollection := mongoClient.Database(config.Mongo.DB).Collection(ProductColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := productCollection.InsertOne(ctx, p)
	return err
}

// Save updates product info
func (p *Product) Save(goz GozContext) error {
	now := time.Now()
	ts := now.Unix()
	p.LastUpdated = ts
	mongoClient := goz.Mongo
	config := goz.Config
	productCollection := mongoClient.Database(config.Mongo.DB).Collection(ProductColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"id": p.ID,
	}
	_, err := productCollection.ReplaceOne(ctx, filter, p)
	return err
}

// List returns products information
func (p *Product) List(goz GozContext) ([]Product, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	productCollection := mongoClient.Database(config.Mongo.DB).Collection(ProductColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	filter := bson.M{
		"subject": p.Subject,
	}
	products := make([]Product, 0)

	cursor, cursorErr := productCollection.Find(ctx, filter)
	if cursorErr == nil {
		for cursor.Next(ctx) {
			var product Product
			cursor.Decode(&product)
			products = append(products, product)
		}
	}
	return products, cursorErr
}

// GetVersions list product versions
func (p *Product) GetVersions(goz GozContext) ([]ProductVersion, error) {
	pSearch := ProductVersion{
		Subject: p.Subject,
		Product: p.ID,
	}
	products, productsErr := pSearch.List(goz)
	return products, productsErr
}

// ProductVersion defines a specific product release matching some package versions
type ProductVersion struct {
	Version      string `json:"version"`
	Description  string `json:"description"`
	ReleaseNotes string `json:"relnotes"`
	Product      string `json:"product"`
	Subject      string `json:"subject"`

	Components []PackageVersion `json:"components"`

	Created     int64             `json:"created"`
	LastUpdated int64             `json:"last_updated"`
	Deleted     bool              `json:"deleted"`
	DeletedTime int64             `json:"deleted_ts"`
	Meta        map[string]string `json:"meta"` // Metadata

}

// List returns product versions information
func (p *ProductVersion) List(goz GozContext) ([]ProductVersion, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	productCollection := mongoClient.Database(config.Mongo.DB).Collection(ProductVersionColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	filter := bson.M{
		"subject": p.Subject,
		"product": p.Product,
	}
	products := make([]ProductVersion, 0)

	cursor, cursorErr := productCollection.Find(ctx, filter)
	if cursorErr == nil {
		for cursor.Next(ctx) {
			var product ProductVersion
			cursor.Decode(&product)
			products = append(products, product)
		}
	}
	return products, cursorErr
}

// Create a product
func (p *ProductVersion) Create(goz GozContext) error {
	now := time.Now()
	ts := now.Unix()
	p.Created = ts
	mongoClient := goz.Mongo
	config := goz.Config
	productCollection := mongoClient.Database(config.Mongo.DB).Collection(ProductVersionColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := productCollection.InsertOne(ctx, p)
	return err
}

// Save updates product info
func (p *ProductVersion) Save(goz GozContext) error {
	now := time.Now()
	ts := now.Unix()
	p.LastUpdated = ts
	mongoClient := goz.Mongo
	config := goz.Config
	productCollection := mongoClient.Database(config.Mongo.DB).Collection(ProductVersionColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"version": p.Version,
	}
	_, err := productCollection.ReplaceOne(ctx, filter, p)
	return err
}

// PackageVersionUpdate is used when a package version is updated
// with new or update files to scan version files
// and update their metadata and update subject quota used size
type PackageVersionUpdate struct {
	ID          primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	Subject     string             `bson:"subject" json:"subject"`
	Repo        string             `bson:"repo" json:"repo"`
	Package     string             `bson:"package" json:"package"`
	Version     string             `bson:"version" json:"version"`
	LastUpdated int64              `bson:"last_updated" json:"last_updated"`
}

// List returns list of pending updates
// Does not handle docker packages
// Handles packages last updated > 1 day (to wait for uploads to be over)
func (p *PackageVersionUpdate) List(goz GozContext) ([]PackageVersionUpdate, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	pCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageVersionUpdateColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	now := time.Now().AddDate(0, 0, -1)
	ts := now.Unix()
	filter := bson.M{
		"last_updated": bson.M{
			"$lte": ts,
		},
	}
	cursor, err := pCollection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	updates := make([]PackageVersionUpdate, 0)
	for cursor.Next(ctx) {
		var pvu PackageVersionUpdate
		cursor.Decode(&pvu)
		log.Debug().Str("subject", pvu.Subject).Str("repo", pvu.Repo).Str("package", pvu.Package).Str("version", pvu.Version).Msg("to scan")
		updates = append(updates, pvu)
	}

	return updates, nil
}

// Scan update files metadata and subject quota used size
func (p *PackageVersionUpdate) Scan(goz GozContext) error {
	packSearch := Package{
		Subject: p.Subject,
		Repo:    p.Repo,
		ID:      p.Package,
	}
	pack, packErr := packSearch.Exists(goz)
	if packErr != nil {
		return packErr
	}
	// Skip docker
	if pack.Type == DockerPackage {
		return nil
	}
	vpackSearch := PackageVersion{
		Subject: p.Subject,
		Repo:    p.Repo,
		Package: p.Package,
		Version: p.Version,
	}
	vpack, vpackErr := vpackSearch.Exists(goz)
	if vpackErr != nil {
		return vpackErr
	}
	log.Debug().Str("subject", p.Subject).Str("repo", p.Repo).Str("package", p.Package).Str("version", p.Version).Msg("scanning")
	_, metaErr := vpack.SetMetaFiles(goz)
	if metaErr != nil {
		log.Error().Str("subject", p.Subject).Str("repo", p.Repo).Str("package", p.Package).Str("version", p.Version).Msg("failed to update metadata")
		return metaErr
	}

	StatQuota(goz, p.Subject)
	delErr := p.Delete(goz)
	if delErr != nil {
		log.Error().Str("id", p.ID.String()).Msg("failed to delete package version update")
	}
	return delErr
}

// New adds a new scan request
func (p *PackageVersionUpdate) New(goz GozContext) error {
	mongoClient := goz.Mongo
	config := goz.Config
	pCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageVersionUpdateColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	now := time.Now()
	ts := now.Unix()
	p.ID = primitive.NewObjectID()
	p.LastUpdated = ts
	_, err := pCollection.InsertOne(ctx, p)
	return err
}

// Delete delete scan request
func (p *PackageVersionUpdate) Delete(goz GozContext) error {
	mongoClient := goz.Mongo
	config := goz.Config
	pCollection := mongoClient.Database(config.Mongo.DB).Collection(PackageVersionUpdateColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"_id": p.ID,
	}
	_, err := pCollection.DeleteOne(ctx, filter)
	return err
}
