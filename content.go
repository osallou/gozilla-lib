package gozillalib

import "fmt"

// Content TODO
type Content struct {
}

// ContentType defines the kind of content
type ContentType int

const (
	// ContentRaw used for simple file management
	ContentRaw ContentType = 0
)

// ContentHandler interface for the different content handlers
type ContentHandler interface {
}

// NewContent returns a content handler based on ContentType
func NewContent(ctx GozContext, kind ContentType) (ContentHandler, error) {
	if kind == ContentRaw {
		return Content{}, nil

	}
	return nil, fmt.Errorf("no content type found")
}
