package gozillalib

import (
	"context"
	"fmt"
	"time"

	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2/bson"
)

// Plan is a pricing plan
type Plan int

const (
	// PlanGuest is a no create plan (user can read and be given repo access but cannot create repo)
	PlanGuest Plan = -1
	// PlanCustomer is a plan for a customer with specific quota rules
	PlanCustomer Plan = 0
)

// String gets textual representation of plan
func (p Plan) String() string {
	switch p {
	case -1:
		return "guest"
	case 0:
		return "customer"
	default:
		return "unknown"
	}

}

// PricingPlan determines the pricing of a plan
type PricingPlan struct {
	ID string `json:"id"`
}

// PlanInfo gives info on a plan
type PlanInfo struct {
	ID          string      `json:"id"`
	Description string      `json:"description"`
	Quota       Quota       `json:"quota"`
	Pricing     PricingPlan `json:"pricing"` // if empty = free
	Kind        Plan        `json:"kind"`    // plan type

	Created     int64 `json:"created"`
	LastUpdated int64 `json:"last_updated"`
}

// UsedBy looks for customers using this plan
func (p *PlanInfo) UsedBy(goz GozContext) ([]Customer, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	custCollection := mongoClient.Database(config.Mongo.DB).Collection(CustomerColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"plan": p.ID,
	}
	customers := make([]Customer, 0)
	cursor, cursorErr := custCollection.Find(ctx, filter)
	if cursorErr == nil {
		for cursor.Next(ctx) {
			var customer Customer
			cursor.Decode(&customer)
			customers = append(customers, customer)
		}
	}
	return customers, cursorErr
}

// List returns all plans
func (p *PlanInfo) List(goz GozContext) ([]PlanInfo, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	planCollection := mongoClient.Database(config.Mongo.DB).Collection(PlanColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{}
	plans := make([]PlanInfo, 0)
	cursor, cursorErr := planCollection.Find(ctx, filter)
	if cursorErr == nil {
		for cursor.Next(ctx) {
			var plan PlanInfo
			cursor.Decode(&plan)
			plans = append(plans, plan)
		}
	}
	return plans, cursorErr
}

// Exists checks if plan exists
func (p *PlanInfo) Exists(goz GozContext) (PlanInfo, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	planCollection := mongoClient.Database(config.Mongo.DB).Collection(PlanColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"id": p.ID,
	}
	var plan PlanInfo
	planErr := planCollection.FindOne(ctx, filter).Decode(&plan)
	return plan, planErr
}

// Create a plan
func (p *PlanInfo) Create(goz GozContext) error {
	now := time.Now()
	ts := now.Unix()
	p.Created = ts
	mongoClient := goz.Mongo
	config := goz.Config
	planCollection := mongoClient.Database(config.Mongo.DB).Collection(PlanColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := planCollection.InsertOne(ctx, p)
	return err
}

// Save updates plan info
func (p *PlanInfo) Save(goz GozContext) error {
	now := time.Now()
	ts := now.Unix()
	p.LastUpdated = ts
	mongoClient := goz.Mongo
	config := goz.Config
	planCollection := mongoClient.Database(config.Mongo.DB).Collection(PlanColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"id": p.ID,
	}
	_, err := planCollection.ReplaceOne(ctx, filter, p)
	return err
}

// Delete removes plan from db
func (p *PlanInfo) Delete(goz GozContext) error {
	pCusts, cErr := p.UsedBy(goz)
	if cErr != nil {
		return cErr
	}
	if len(pCusts) > 0 {
		return fmt.Errorf("cannot delete plan, plan has customers")
	}
	mongoClient := goz.Mongo
	config := goz.Config
	planCollection := mongoClient.Database(config.Mongo.DB).Collection(PlanColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := planCollection.DeleteOne(ctx, bson.M{
		"id": p.ID,
	})
	return err
}

// PlanHistory contains information about a plan usage (from/to date and plan info)
type PlanHistory struct {
	Plan PlanInfo `json:"plan"`
	From int64    `json:"from"`
	To   int64    `json:"to"`
}

// Customer is needed for billing
type Customer struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Address     string `json:"address"`
	City        string `json:"city"`
	Country     string `json:"country"`
	Email       string `json:"email"`
	Phone       string `json:"phone"`
	Plan        string `json:"plan"` // name of plan, if empty, use *free* plan

	History []PlanHistory `json:"history"`

	Created     int64 `json:"created"`
	LastUpdated int64 `json:"last_updated"`
}

// Exists checks if customer exists
func (c *Customer) Exists(goz GozContext) (Customer, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	custCollection := mongoClient.Database(config.Mongo.DB).Collection(CustomerColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"id": c.ID,
	}
	var customer Customer
	custErr := custCollection.FindOne(ctx, filter).Decode(&customer)
	return customer, custErr
}

// Create a customer
func (c *Customer) Create(goz GozContext) error {
	now := time.Now()
	ts := now.Unix()
	c.Created = ts
	mongoClient := goz.Mongo
	config := goz.Config
	custCollection := mongoClient.Database(config.Mongo.DB).Collection(CustomerColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := custCollection.InsertOne(ctx, c)
	return err
}

// Save updates customer info
func (c *Customer) Save(goz GozContext) error {
	now := time.Now()
	ts := now.Unix()
	c.LastUpdated = ts
	mongoClient := goz.Mongo
	config := goz.Config
	custCollection := mongoClient.Database(config.Mongo.DB).Collection(CustomerColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"id": c.ID,
	}
	_, err := custCollection.ReplaceOne(ctx, filter, c)
	return err
}

// Users returns users linked to customer
func (c *Customer) Users(goz GozContext) ([]User, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	userCollection := mongoClient.Database(config.Mongo.DB).Collection(UserColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"customer": c.ID,
	}
	users := make([]User, 0)
	cursor, cursorErr := userCollection.Find(ctx, filter)
	if cursorErr == nil {
		for cursor.Next(ctx) {
			var user User
			cursor.Decode(&user)
			users = append(users, user)
		}
	}
	return users, cursorErr
}

// Delete removes customer from db
func (c *Customer) Delete(goz GozContext) error {
	cUsers, cErr := c.Users(goz)
	if cErr != nil {
		return cErr
	}
	if len(cUsers) > 0 {
		return fmt.Errorf("cannot delete customer, customer has users")
	}
	mongoClient := goz.Mongo
	config := goz.Config
	custCollection := mongoClient.Database(config.Mongo.DB).Collection(CustomerColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := custCollection.DeleteOne(ctx, bson.M{
		"id": c.ID,
	})
	return err
}

// List returns all customers
func (c *Customer) List(goz GozContext) ([]Customer, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	custCollection := mongoClient.Database(config.Mongo.DB).Collection(CustomerColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{}
	customers := make([]Customer, 0)
	cursor, cursorErr := custCollection.Find(ctx, filter)
	if cursorErr == nil {
		for cursor.Next(ctx) {
			var customer Customer
			cursor.Decode(&customer)
			customers = append(customers, customer)
		}
	}
	return customers, cursorErr
}

// GetNbSubjects counts number of subjects owned by customer
func (c *Customer) GetNbSubjects(goz GozContext) (int64, error) {
	_, cErr := c.Exists(goz)
	if cErr != nil {
		return 0, cErr
	}
	mongoClient := goz.Mongo
	config := goz.Config
	subjectCollection := mongoClient.Database(config.Mongo.DB).Collection(SubjectColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"customer": c.ID,
	}
	nb, nbErr := subjectCollection.CountDocuments(ctx, filter)
	if nbErr != nil {
		return 0, nbErr
	}
	return nb, nil
}

// OwnedSubjects get repos owner by customer (org subjects)
func (c *Customer) OwnedSubjects(goz GozContext) ([]Subject, error) {
	subjectCollection := goz.Mongo.Database(goz.Config.Mongo.DB).Collection(SubjectColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"customer": c.ID,
	}
	subjects := make([]Subject, 0)
	cursor, cursorErr := subjectCollection.Find(ctx, filter)
	if cursorErr == nil {
		for cursor.Next(ctx) {
			var subject Subject
			cursor.Decode(&subject)
			if subject.Type != SubjectPersonal {
				subjects = append(subjects, subject)
			}
		}
	}

	return subjects, nil
}

// Quota manage subject quotas
type Quota struct {
	Subjects int    `json:"subjects"` // Max number of subjects, 0 = subject creation only by operators, not users. Personal subject is automatically created
	Size     string `json:"size"`     // Max size of repo (1G, 100M) 0 = unlimited
}

// UserProfile type of type of user profile
type UserProfile int

const (
	// UserProfileUser is a standard user, default
	UserProfileUser UserProfile = 0
	// UserProfileOperator has some super priviledges
	UserProfileOperator UserProfile = 10
	// UserProfileAdmin is a site admin
	UserProfileAdmin UserProfile = 20
)

// LogMode defines how user logged in
type LogMode int

const (
	// LogAnonymous not logged, i.e. anonymous
	LogAnonymous LogMode = -1
	// LogAPIKey user logged with api key
	LogAPIKey LogMode = 0
	// LogPassword user logged with password
	LogPassword LogMode = 1
)

// UserStatus indicates user active/inactive status
type UserStatus int

const (
	// UserActive  user is active and can access api
	UserActive UserStatus = 0
	// UserInactive user is deactivated and cannot access api
	UserInactive UserStatus = 1
)

// User describes a gozilla user
type User struct {
	ID       string `json:"id"`
	Email    string `json:"email"`
	Origin   string `json:"origin"` // User origin (local, ldap, cas:casid, openid:google, ...)
	Password string `json:"password"`
	APIKey   string `json:"apikey"`
	//Admin       bool        `json:"admin"`    // Is site admin
	Customer    string      `json:"customer"` // Customer id, if empty, will use Guest plan
	Profile     UserProfile `json:"profile"`  // admin, operator, user
	Created     int64       `json:"created"`
	LastUpdated int64       `json:"last_updated"`
	Status      UserStatus  `json:"status"` // 0: user active, 1: user deactivated
	LoginMode   LogMode     `bson:"-"`      // Log via apikey or password
}

// Active checks user status
func (u *User) Active() bool {
	if u.Status == 0 {
		return true
	}
	return false
}

// OwnedRepos get repos owner by user
func (u *User) OwnedRepos(goz GozContext) ([]Repo, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	repoCollection := mongoClient.Database(config.Mongo.DB).Collection(RepoColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"owner": u.ID,
	}
	repos := make([]Repo, 0)
	cursor, cursorErr := repoCollection.Find(ctx, filter)
	if cursorErr == nil {
		for cursor.Next(ctx) {
			var repo Repo
			cursor.Decode(&repo)
			repos = append(repos, repo)
		}
	}
	return repos, cursorErr
}

// OwnedSubjects get subjects owner by user
func (u *User) OwnedSubjects(goz GozContext) ([]Subject, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	subjectCollection := mongoClient.Database(config.Mongo.DB).Collection(SubjectColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"owner": u.ID,
	}
	subjects := make([]Subject, 0)
	cursor, cursorErr := subjectCollection.Find(ctx, filter)
	if cursorErr == nil {
		for cursor.Next(ctx) {
			var subject Subject
			cursor.Decode(&subject)
			subjects = append(subjects, subject)
		}
	}
	return subjects, cursorErr
}

// GetPlan returns user plan based on its customer, if no customer or plan defined, use *Guest* plan
func (u *User) GetPlan(goz GozContext) (PlanInfo, error) {
	if u.Customer == "" {
		return PlanInfo{
			Kind: PlanGuest,
		}, nil
	}
	customerSearch := Customer{
		ID: u.Customer,
	}
	customer, cErr := customerSearch.Exists(goz)
	if cErr != nil {
		return PlanInfo{
			Kind: PlanGuest,
		}, fmt.Errorf("customer not found")
	}
	planSearch := PlanInfo{
		ID: customer.Plan,
	}
	plan, pErr := planSearch.Exists(goz)
	if pErr != nil {
		return PlanInfo{
			Kind: PlanGuest,
		}, fmt.Errorf("plan not found")
	}
	return plan, nil
}

// IsAdmin checks if user is admin
func (u *User) IsAdmin() bool {
	if u.Profile == UserProfileAdmin {
		return true
	}
	return false
}

// IsOperator checks if user is an operator or admin
func (u *User) IsOperator() bool {
	if u.IsAdmin() {
		return true
	}
	if u.Profile == UserProfileOperator {
		return true
	}
	return false
}

// Exists checks if user exists
func (u *User) Exists(goz GozContext) (User, error) {
	mongoClient := goz.Mongo
	config := goz.Config
	userCollection := mongoClient.Database(config.Mongo.DB).Collection(UserColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"id": u.ID,
	}
	var user User
	userErr := userCollection.FindOne(ctx, filter).Decode(&user)
	return user, userErr
}

// Create a user
func (u *User) Create(goz GozContext) error {
	if _, err := u.Exists(goz); err == nil {
		return fmt.Errorf("user already exists")
	}
	now := time.Now()
	ts := now.Unix()
	u.Created = ts
	if u.APIKey == "" {
		u.APIKey = Random(8)
	}
	mongoClient := goz.Mongo
	config := goz.Config
	userCollection := mongoClient.Database(config.Mongo.DB).Collection(UserColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := userCollection.InsertOne(ctx, u)
	return err
}

// Register creates a new users
func (u *User) Register(goz GozContext) error {
	if u.Password == "" {
		return fmt.Errorf("no password for user")
	}
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	u.Password = string(hashedPassword)
	u.APIKey = Random(8)
	err := u.Create(goz)
	return err
}

// Save updates user info
func (u *User) Save(goz GozContext) error {
	now := time.Now()
	ts := now.Unix()
	u.LastUpdated = ts
	mongoClient := goz.Mongo
	config := goz.Config
	userCollection := mongoClient.Database(config.Mongo.DB).Collection(UserColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"id": u.ID,
	}
	_, err := userCollection.ReplaceOne(ctx, filter, u)
	return err
}

// Activate reactive a user
func (u *User) Activate(goz GozContext) error {
	now := time.Now()
	ts := now.Unix()
	u.LastUpdated = ts
	mongoClient := goz.Mongo
	config := goz.Config
	userCollection := mongoClient.Database(config.Mongo.DB).Collection(UserColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"id": u.ID,
	}
	update := bson.M{
		"$set": bson.M{
			"status":       0,
			"last_updated": ts,
		},
	}
	_, err := userCollection.UpdateOne(ctx, filter, update)
	return err
}

// Deactivate deactivate a user
func (u *User) Deactivate(goz GozContext) error {
	now := time.Now()
	ts := now.Unix()
	u.LastUpdated = ts
	mongoClient := goz.Mongo
	config := goz.Config
	userCollection := mongoClient.Database(config.Mongo.DB).Collection(UserColl)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"id": u.ID,
	}
	update := bson.M{
		"$set": bson.M{
			"status":       1,
			"last_updated": ts,
		},
	}
	_, err := userCollection.UpdateOne(ctx, filter, update)
	return err
}

// UserAuth authenticate user with his api key
func UserAuth(goz GozContext, login string, apikey string) (user User, err error) {
	user = User{ID: login}
	userDB, userErr := user.Exists(goz)
	if userErr == nil {
		if userDB.APIKey != apikey {
			return user, fmt.Errorf("invalid API key")
		}
		if !userDB.Active() {
			return user, fmt.Errorf("user not active")
		}
	}
	userDB.Password = "****"
	userDB.LoginMode = LogAPIKey
	return userDB, userErr
}

// UserBind authenticate user against configured authentication systems
func UserBind(goz GozContext, login string, password string) (user User, err error) {
	user = User{ID: login}
	userDB, userErr := user.Exists(goz)
	if userErr == nil {
		err := bcrypt.CompareHashAndPassword([]byte(userDB.Password), []byte(password))
		if err != nil {
			return user, fmt.Errorf("invalid login/password")
		}
		if !userDB.Active() {
			return user, fmt.Errorf("user not active")
		}
	}
	userDB.Password = "****"
	userDB.LoginMode = LogPassword
	return userDB, userErr
}
