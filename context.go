package gozillalib

import (
	elasticsearch "github.com/elastic/go-elasticsearch"
	mongo "go.mongodb.org/mongo-driver/mongo"
)

const (
	// UserColl users collection
	UserColl string = "user"
	// CustomerColl customers collection
	CustomerColl string = "customer"
	// PlanColl plan collection
	PlanColl string = "plan"
	// SubjectColl subjects collection
	SubjectColl string = "subject"
	// RepoColl repositories collection
	RepoColl string = "repo"
	// PackageColl package collection
	PackageColl string = "package"
	// PackageVersionColl package collection
	PackageVersionColl string = "packversion"
	// ProductColl product collection
	ProductColl string = "product"
	// ProductVersionColl product version collection
	ProductVersionColl string = "vproduct"
	// PackageVersionUpdateColl update collection
	PackageVersionUpdateColl string = "vpackupdate"
)

// GozContext is used to forwarded context data to requests
type GozContext struct {
	User        User // Id of connected user, empty if not connected
	Mongo       *mongo.Client
	Config      Config
	Storage     StorageHandler
	AmqpHandler *AmqpHandler
	Elastic     *elasticsearch.Client
}
