package gozillalib

import (
	"context"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"path"
	"strings"
	"time"

	"github.com/gophercloud/gophercloud/openstack/objectstorage/v1/accounts"

	"github.com/elastic/go-elasticsearch/esapi"
	"github.com/rs/zerolog/log"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"github.com/gophercloud/gophercloud/openstack/objectstorage/v1/containers"
	"github.com/gophercloud/gophercloud/openstack/objectstorage/v1/objects"

	"github.com/dustin/go-humanize"
)

// FileACL allowed acls
type FileACL int

const (
	// FileACLNone no access
	FileACLNone FileACL = 0
	// FileACLRead read only
	FileACLRead FileACL = 1
	// FileACLWrite read/write/delete
	FileACLWrite FileACL = 2
)

// FileToken is token message allowing file read/write
type FileToken struct {
	Path   string
	Access FileACL
	Expire int64
}

// FileInfo represent a file
type FileInfo struct {
	LastUpdated int64  `json:"last_updated"`
	Size        int64  `json:"size"`
	MD5         string `json:"md5"`
}

// FileObject is a generic file object interface
type FileObject interface {
	GetSubject() string
	GetRepo() string
	GetPackage() string
	GetPackageType() PackageType
	GetVersion() string
	SetVersion(v string)
	GetName() string
	GetType() PackageType
	SetType(t PackageType)
	GetPath() string
	SetName(n string)
	Index(goz GozContext) error
	UnIndex(goz GozContext) error
	SetMeta(m FileInfo)
	GetMeta() FileInfo
	GetExtras() map[string]interface{}
	SetExtra(string, interface{})
	SetExtras(map[string]interface{})
	ShouldSkipIndex() bool
	DoSkipIndex(bool)
}

// RawFileObject describes a raw file
type RawFileObject struct {
	Subject     string
	Repo        string
	Package     string
	PackageType PackageType
	Version     string
	//Request     *http.Request
	Name      string
	Path      string
	Meta      FileInfo
	Extras    map[string]interface{}
	SkipIndex bool // Index or not file

	IndexPackageVersion string `json:"ivpack"`
	IndexPackage        string `json:"ipack"`
	IndexRepo           string `json:"irepo"`
}

// DoSkipIndex TODO
func (f *RawFileObject) DoSkipIndex(skip bool) {
	f.SkipIndex = skip
}

// ShouldSkipIndex TODO
func (f *RawFileObject) ShouldSkipIndex() bool {
	if strings.HasSuffix(f.GetSubject(), "_segments") {
		f.SkipIndex = true
	}
	return f.SkipIndex
}

// SetExtra add extra about file
func (f *RawFileObject) SetExtra(key string, value interface{}) {
	if f.Extras == nil {
		f.Extras = make(map[string]interface{})
	}
	f.Extras[key] = value
}

// SetExtras add extra about file
func (f *RawFileObject) SetExtras(m map[string]interface{}) {
	f.Extras = m
}

// GetExtras return extra about file
func (f *RawFileObject) GetExtras() map[string]interface{} {
	if f.Extras == nil {
		return make(map[string]interface{})
	}
	return f.Extras
}

// SetMeta add metadata about file
func (f *RawFileObject) SetMeta(m FileInfo) {
	f.Meta = m
}

// GetMeta add metadata about file
func (f *RawFileObject) GetMeta() FileInfo {
	return f.Meta
}

// GetType TODO
func (f *RawFileObject) GetType() PackageType {
	return f.PackageType
}

// SetType TODO
func (f *RawFileObject) SetType(t PackageType) {
	f.PackageType = t
}

// Index adds file info in elasticsearch
func (f *RpmRepoFileObject) Index(goz GozContext) error {
	f.Path = f.GetPath()
	err := f.RawFileObject.Index(goz)
	return err
}

// UnIndex remove file from index
func (f *RpmRepoFileObject) UnIndex(goz GozContext) error {
	f.Path = f.GetPath()
	err := f.RawFileObject.UnIndex(goz)
	return err
}

// Index adds file info in elasticsearch
func (f *RpmFileObject) Index(goz GozContext) error {
	f.Path = f.GetPath()
	err := f.RawFileObject.Index(goz)
	return err
}

// UnIndex remove file from index
func (f *RpmFileObject) UnIndex(goz GozContext) error {
	f.Path = f.GetPath()
	err := f.RawFileObject.UnIndex(goz)
	return err
}

// Index adds file info in elasticsearch
func (f *DebianRepoFileObject) Index(goz GozContext) error {
	f.Path = f.GetPath()
	err := f.RawFileObject.Index(goz)
	return err
}

// UnIndex remove file from index
func (f *DebianRepoFileObject) UnIndex(goz GozContext) error {
	f.Path = f.GetPath()
	err := f.RawFileObject.UnIndex(goz)
	return err
}

// Index adds file info in elasticsearch
func (f *DebianFileObject) Index(goz GozContext) error {
	f.Path = f.GetPath()
	err := f.RawFileObject.Index(goz)
	return err
}

// UnIndex remove file from index
func (f *DebianFileObject) UnIndex(goz GozContext) error {
	f.Path = f.GetPath()
	err := f.RawFileObject.UnIndex(goz)
	return err
}

// Index adds file info in elasticsearch
func (f *MavenFileObject) Index(goz GozContext) error {
	f.Path = f.GetPath()
	err := f.RawFileObject.Index(goz)
	return err
}

// UnIndex remove file from index
func (f *MavenFileObject) UnIndex(goz GozContext) error {
	f.Path = f.GetPath()
	err := f.RawFileObject.UnIndex(goz)
	return err
}

// Index adds file info in elasticsearch
func (f *ConanFileObject) Index(goz GozContext) error {
	f.Path = f.GetPath()
	err := f.RawFileObject.Index(goz)
	return err
}

// UnIndex remove file from index
func (f *ConanFileObject) UnIndex(goz GozContext) error {
	f.Path = f.GetPath()
	err := f.RawFileObject.UnIndex(goz)
	return err
}

// Index adds file info in elasticsearch
func (f *RawFileObject) Index(goz GozContext) error {
	if f.ShouldSkipIndex() {
		return nil
	}
	if f.Path == "" {
		f.Path = f.GetPath()
	}

	f.IndexRepo = fmt.Sprintf("%s_%s", f.GetSubject(), f.GetRepo())
	f.IndexPackage = fmt.Sprintf("%s_%s_%s", f.GetSubject(), f.GetRepo(), f.GetPackage())
	f.IndexPackageVersion = fmt.Sprintf("%s_%s_%s_%s", f.GetSubject(), f.GetRepo(), f.GetPackage(), f.GetVersion())

	log.Debug().Msgf("Index %+v", f)
	//f.Path = f.GetPath()
	//fJSON, _ := json.Marshal(f)
	fJSON, _ := json.Marshal(f)
	b64Id := b64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s-%s-%s-%s-%s", f.Subject, f.Repo, f.Package, f.Version, f.Path)))
	//log.Debug().Msgf("index %s", fmt.Sprintf("%s-%s-%s-%s-%s", f.Subject, f.Repo, f.Package, f.Version, f.Name))
	req := esapi.IndexRequest{
		Index:      "gozfile",
		DocumentID: string(b64Id),
		Body:       strings.NewReader(string(fJSON)),
		Refresh:    "true",
	}

	res, err := req.Do(context.Background(), goz.Elastic)

	if err != nil {
		log.Error().Msgf("Error getting index response: %s", err)
		return err
	}
	defer res.Body.Close()
	if res.IsError() {
		log.Error().Msgf("index error %s", res.String())
		return fmt.Errorf("%s", res.String())

	}
	log.Debug().Str("file", f.GetPath()).Msg("index file")

	return nil
}

// UnIndex remove file from index
func (f *RawFileObject) UnIndex(goz GozContext) error {
	if f.ShouldSkipIndex() {
		return nil
	}

	if f.Path == "" {
		f.Path = f.GetPath()
	}

	f.IndexRepo = fmt.Sprintf("%s_%s", f.GetSubject(), f.GetRepo())
	f.IndexPackage = fmt.Sprintf("%s_%s_%s", f.GetSubject(), f.GetRepo(), f.GetPackage())
	f.IndexPackageVersion = fmt.Sprintf("%s_%s_%s_%s", f.GetSubject(), f.GetRepo(), f.GetPackage(), f.GetVersion())

	log.Debug().Msgf("Remove from index %+v", f)

	b64Id := b64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s-%s-%s-%s-%s", f.Subject, f.Repo, f.Package, f.Version, f.Path)))
	//log.Debug().Msgf("index %s", fmt.Sprintf("%s-%s-%s-%s-%s", f.Subject, f.Repo, f.Package, f.Version, f.Name))
	req := esapi.DeleteRequest{
		Index:      "gozfile",
		DocumentID: string(b64Id),
		Refresh:    "true",
	}

	res, err := req.Do(context.Background(), goz.Elastic)

	if err != nil {
		log.Error().Msgf("Error getting index response: %s", err)
		return err
	}
	defer res.Body.Close()
	if res.IsError() {
		log.Error().Msgf("index error %s", res.String())
		return fmt.Errorf("%s", res.String())

	}
	log.Debug().Str("file", f.GetPath()).Msg("unindex file")

	return nil
}

// GetSubject TODO
func (f *RawFileObject) GetSubject() string {
	return f.Subject
}

// GetRepo TODO
func (f *RawFileObject) GetRepo() string {
	return f.Repo
}

// GetPackage TODO
func (f *RawFileObject) GetPackage() string {
	return f.Package
}

// GetPackageType TODO
func (f *RawFileObject) GetPackageType() PackageType {
	return f.PackageType
}

// GetVersion TODO
func (f *RawFileObject) GetVersion() string {
	return f.Version
}

// GetName TODO
func (f *RawFileObject) GetName() string {
	return f.Name
}

// SetVersion TODO
func (f *RawFileObject) SetVersion(v string) {
	f.Version = v
}

// SetName TODO
func (f *RawFileObject) SetName(n string) {
	f.Name = n
}

// NewRawFileObject gets a raw FileObject
func NewRawFileObject(subject, repo, pack, version, name string) FileObject {
	f := RawFileObject{
		Subject: subject,
		Repo:    repo,
		Package: pack,
		Version: version,
		Name:    name,
	}
	return &f
}

// NewConanFileObject gets a conan FileObject
func NewConanFileObject(subject, repo, pack, version, name string) FileObject {
	f := ConanFileObject{
		RawFileObject{
			Subject:     subject,
			Repo:        repo,
			Package:     pack,
			Version:     version,
			Name:        name,
			PackageType: ConanPackage,
		},
	}
	f.Path = f.GetPath()
	return &f
}

// ConanFileObject describes a maven file
type ConanFileObject struct {
	RawFileObject
}

// GetPath builds file location
func (f *ConanFileObject) GetPath() string {
	rrev := "0"
	if extraRREV, ok := f.Extras["rrev"]; ok {
		rrev = extraRREV.(string)
	}

	fileFullPath := path.Join(f.Subject, f.Repo, "conan", rrev, "export", f.Name)

	if packid, ok := f.Extras["packid"].(string); ok {
		prev := "0"
		if extraPREV, ok := f.Extras["prev"]; ok {
			prev = extraPREV.(string)
		}
		fileFullPath = path.Join(f.Subject, f.Repo, "conan", rrev, "package", packid, prev, f.Name)
	}

	return fileFullPath
}

// NewMavenFileObject gets a maven FileObject
func NewMavenFileObject(subject, repo, pack, version, name string) FileObject {
	f := MavenFileObject{
		RawFileObject{
			Subject:     subject,
			Repo:        repo,
			Package:     pack,
			Version:     version,
			Name:        name,
			PackageType: MavenPackage,
		},
	}
	f.Path = f.GetPath()
	return &f
}

// MavenFileObject describes a maven file
type MavenFileObject struct {
	RawFileObject
}

// GetPath builds file location
func (f *MavenFileObject) GetPath() string {
	fileFullPath := path.Join(f.Subject, f.Repo, "maven", f.Name)
	return fileFullPath
}

// NewDebianFileObject gets a debian FileObject
func NewDebianFileObject(subject, repo, pack, version, name string) FileObject {
	f := DebianFileObject{
		RawFileObject{
			Subject:     subject,
			Repo:        repo,
			Package:     pack,
			Version:     version,
			Name:        name,
			PackageType: DebianPackage,
		},
	}
	f.Path = f.GetPath()
	return &f
}

// NewDebianRepoFileObject gets a debian FileObject
func NewDebianRepoFileObject(subject, repo, pack, name string) FileObject {
	f := DebianRepoFileObject{
		RawFileObject{
			Subject:     subject,
			Repo:        repo,
			Package:     pack,
			Name:        name,
			PackageType: DebianPackage,
		},
	}
	f.Path = f.GetPath()
	return &f
}

// DebianRepoFileObject describes a debian repo file
type DebianRepoFileObject struct {
	RawFileObject
}

// GetPath builds file location
func (f *DebianRepoFileObject) GetPath() string {
	fileFullPath := path.Join(f.Subject, f.Repo, "debian", f.Package, f.Name)
	return fileFullPath
}

// DebianFileObject describes a debian file
type DebianFileObject struct {
	RawFileObject
}

// RpmFileObject describes a rpm file
type RpmFileObject struct {
	RawFileObject
}

// NewRpmFileObject gets a rpm FileObject
func NewRpmFileObject(subject, repo, pack, version, name string) FileObject {
	f := RpmFileObject{
		RawFileObject{
			Subject:     subject,
			Repo:        repo,
			Package:     pack,
			Version:     version,
			Name:        name,
			PackageType: RpmPackage,
		},
	}
	f.Path = f.GetPath()
	return &f
}

// NewRpmRepoFileObject gets a rpm FileObject
func NewRpmRepoFileObject(subject, repo, pack, name string) FileObject {
	f := RpmRepoFileObject{
		RawFileObject{
			Subject:     subject,
			Repo:        repo,
			Package:     pack,
			Name:        name,
			PackageType: RpmPackage,
		},
	}
	f.Path = f.GetPath()
	return &f
}

// RpmRepoFileObject describes a rpm repo file
type RpmRepoFileObject struct {
	RawFileObject
}

// GetPath builds file location
func (f *RpmRepoFileObject) GetPath() string {
	fileFullPath := path.Join(f.Subject, f.Repo, "rpm", f.Package, f.Name)
	return fileFullPath
}

// GetPath builds file location
func (f *RawFileObject) GetPath() string {
	fileFullPath := path.Join(f.Subject, f.Repo, "raw", f.Package, f.Version, f.Name)
	return fileFullPath
}

// StorageHandler interface to storage managers
type StorageHandler interface {
	CreateBucket(goz GozContext, bucket string) error
	SetSecretKey(goz GozContext, bucket string) error
	CreateRepo(goz GozContext, bucket string, repo string) error
	GetObjectURL(goz GozContext, f FileObject, expireMinutes int64) (string, error) // url to object
	SaveObject(goz GozContext, f FileObject, expireMinutes int64) (string, error)
	DeleteObject(goz GozContext, f FileObject, expireMinutes int64) (string, error)
	MD5(goz GozContext, f FileObject) (FileInfo, error)
	SetQuota(goz GozContext, bucket string, plan PlanInfo) error
	GetQuota(goz GozContext, bucket string) (int64, error)
}

// NewStorageHandler gets a storage handler according to config
func NewStorageHandler(cfg Config) (StorageHandler, error) {
	if cfg.Storage.Type == LocalStorage {
		h := LocalFileHandler{}
		return h, nil
	}
	if cfg.Storage.Type == SwiftStorage {
		h := SwiftHandler{}
		return h, nil
	}
	return nil, fmt.Errorf("no storage handler defined")
}

// LocalFileHandler handles local storage files
type LocalFileHandler struct {
}

// SwiftHandler TODO
type SwiftHandler struct {
	LocalFileHandler
	provider *gophercloud.ProviderClient
}

// SetQuota update quota for bucket according to plan
func (h LocalFileHandler) SetQuota(goz GozContext, bucket string, plan PlanInfo) error {
	return fmt.Errorf("quotas not handled by local storage")
}

// GetQuota returns used quota for bucket
func (h LocalFileHandler) GetQuota(goz GozContext, bucket string) (int64, error) {
	return 0, fmt.Errorf("quotas not handled by local storage")
}

// GetObjectURL TODO
func (h LocalFileHandler) GetObjectURL(goz GozContext, f FileObject, expireMinutes int64) (string, error) {
	var ts int64
	expireAt := time.Now().Add(time.Duration(expireMinutes) * time.Minute)
	ts = expireAt.Unix()
	fileToken := FileToken{
		Path:   f.GetPath(),
		Access: FileACLRead,
		Expire: ts,
	}
	msg, _ := json.Marshal(fileToken)
	token, _ := FernetEncode(goz.Config, msg)

	redirect := fmt.Sprintf("%s/%s?token=%s", goz.Config.Storage.URL, f.GetPath(), token)
	return redirect, nil
}

// SaveObject TODO
func (h LocalFileHandler) SaveObject(goz GozContext, f FileObject, expireMinutes int64) (string, error) {
	var ts int64
	expireAt := time.Now().Add(time.Duration(expireMinutes) * time.Minute)
	ts = expireAt.Unix()
	fileToken := FileToken{
		Path:   f.GetPath(),
		Access: FileACLWrite,
		Expire: ts,
	}
	msg, _ := json.Marshal(fileToken)
	token, _ := FernetEncode(goz.Config, msg)

	redirect := fmt.Sprintf("%s%s/%s?token=%s", goz.Config.Storage.URL, goz.Config.Storage.Prefix, f.GetPath(), token)
	return redirect, nil
}

// DeleteObject TODO
func (h LocalFileHandler) DeleteObject(goz GozContext, f FileObject, expireMinutes int64) (string, error) {
	var ts int64
	expireAt := time.Now().Add(time.Duration(expireMinutes) * time.Minute)
	ts = expireAt.Unix()
	fileToken := FileToken{
		Path:   f.GetPath(),
		Access: FileACLWrite,
		Expire: ts,
	}
	msg, _ := json.Marshal(fileToken)
	token, _ := FernetEncode(goz.Config, msg)

	redirect := fmt.Sprintf("%s%s/%s?token=%s", goz.Config.Storage.URL, goz.Config.Storage.Prefix, f.GetPath(), token)
	return redirect, nil
}

// MD5 gets the md5sum of requested file
func (h LocalFileHandler) MD5(goz GozContext, f FileObject) (FileInfo, error) {
	fileInfo := FileInfo{}

	var ts int64
	expireAt := time.Now().Add(time.Duration(5) * time.Minute)
	ts = expireAt.Unix()
	fileToken := FileToken{
		Path:   "stat/" + f.GetPath(),
		Access: FileACLRead,
		Expire: ts,
	}
	msg, _ := json.Marshal(fileToken)
	token, _ := FernetEncode(goz.Config, msg)
	redirect := fmt.Sprintf("%s%s/stat/%s?token=%s", goz.Config.Storage.URL, goz.Config.Storage.Prefix, f.GetPath(), token)

	resp, err := http.Get(redirect)
	if err != nil {
		return fileInfo, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fileInfo, err
	}
	json.Unmarshal(body, &fileInfo)
	return fileInfo, nil

}

// SetSecretKey update bucket secret key
// does nothing, do not applies to local handler
func (h LocalFileHandler) SetSecretKey(goz GozContext, bucket string) error {
	return nil
}

// CreateBucket creates a bucket
func (h LocalFileHandler) CreateBucket(goz GozContext, bucket string) error {
	//log.Debug().Str("bucket", bucket).Str("dir", h.BaseDir).Msg("create bucket")
	//return os.MkdirAll(path.Join(h.BaseDir, bucket), os.ModePerm)

	return nil
}

// CreateRepo creates repository in bucket
func (h LocalFileHandler) CreateRepo(goz GozContext, bucket string, repo string) error {
	//log.Debug().Str("repo", repo).Str("bucket", bucket).Str("dir", h.BaseDir).Msg("create repo")
	//return os.MkdirAll(path.Join(h.BaseDir, bucket, repo), os.ModePerm)

	return nil
}

// Swift handler

// Init sets up openstack client
func (h *SwiftHandler) Init(goz GozContext) error {
	if h.provider == nil {
		opts := gophercloud.AuthOptions{
			TenantID:         goz.Config.Storage.Openstack.Project,
			DomainID:         goz.Config.Storage.Openstack.Domain,
			IdentityEndpoint: goz.Config.Storage.URL,
			Username:         goz.Config.Storage.Openstack.AccessKey,
			Password:         goz.Config.Storage.Openstack.AccessPassword,
		}
		provider, err := openstack.AuthenticatedClient(opts)
		if err != nil {
			log.Error().Err(err).Msg("failed to set openstack auth provider")
			return err
		}
		h.provider = provider
	}
	return nil
}

// GetObjectURL TODO
func (h SwiftHandler) GetObjectURL(goz GozContext, f FileObject, expireMinutes int64) (string, error) {
	h.Init(goz)
	storeClient, storeErr := openstack.NewObjectStorageV1(h.provider, gophercloud.EndpointOpts{
		Region:       goz.Config.Storage.Openstack.Region,
		Availability: gophercloud.AvailabilityPublic,
	})
	if storeErr != nil {
		log.Error().Err(storeErr).Msg("auth failed")
		return "", storeErr
	}
	opts := objects.CreateTempURLOpts{
		Method: "GET",
		TTL:    3600,
	}
	rpath := strings.Split(f.GetPath(), "/")
	newpath := strings.Join(rpath[1:], "/")
	redirect, redirectErr := objects.CreateTempURL(storeClient, f.GetSubject(), newpath, opts)
	return redirect, redirectErr
}

// SaveObject TODO
func (h SwiftHandler) SaveObject(goz GozContext, f FileObject, expireMinutes int64) (string, error) {
	h.Init(goz)
	storeClient, storeErr := openstack.NewObjectStorageV1(h.provider, gophercloud.EndpointOpts{
		Region:       goz.Config.Storage.Openstack.Region,
		Availability: gophercloud.AvailabilityPublic,
	})
	if storeErr != nil {
		log.Error().Err(storeErr).Msg("auth failed")
		return "", storeErr
	}
	opts := objects.CreateTempURLOpts{
		Method: "PUT",
		TTL:    3600,
	}
	rpath := strings.Split(f.GetPath(), "/")
	newpath := strings.Join(rpath[1:], "/")
	redirect, redirectErr := objects.CreateTempURL(storeClient, f.GetSubject(), newpath, opts)
	return redirect, redirectErr
}

// DeleteObject TODO
func (h SwiftHandler) DeleteObject(goz GozContext, f FileObject, expireMinutes int64) (string, error) {
	h.Init(goz)
	storeClient, storeErr := openstack.NewObjectStorageV1(h.provider, gophercloud.EndpointOpts{
		Region:       goz.Config.Storage.Openstack.Region,
		Availability: gophercloud.AvailabilityPublic,
	})
	if storeErr != nil {
		log.Error().Err(storeErr).Msg("auth failed")
		return "", storeErr
	}
	opts := objects.CreateTempURLOpts{
		Method: "DELETE",
		TTL:    3600,
	}
	rpath := strings.Split(f.GetPath(), "/")
	newpath := strings.Join(rpath[1:], "/")
	redirect, redirectErr := objects.CreateTempURL(storeClient, f.GetSubject(), newpath, opts)
	return redirect, redirectErr
}

// MD5 gets the md5sum of requested file
func (h SwiftHandler) MD5(goz GozContext, f FileObject) (FileInfo, error) {
	h.Init(goz)

	storeClient, storeErr := openstack.NewObjectStorageV1(h.provider, gophercloud.EndpointOpts{
		Region:       goz.Config.Storage.Openstack.Region,
		Availability: gophercloud.AvailabilityPublic,
	})
	if storeErr != nil {
		log.Error().Err(storeErr).Msg("auth failed")
		return FileInfo{}, storeErr
	}

	rpath := strings.Split(f.GetPath(), "/")
	newpath := strings.Join(rpath[1:], "/")
	res, resErr := objects.Get(storeClient, f.GetSubject(), newpath, objects.GetOpts{}).Extract()
	if resErr != nil {
		log.Error().Msgf("GET INFO %s :   %s", f.GetPath(), resErr)
		log.Error().Err(storeErr).Msg("extract info failed")
		return FileInfo{}, storeErr
	}

	fileInfo := FileInfo{
		MD5:         res.ETag,
		Size:        res.ContentLength,
		LastUpdated: res.LastModified.Unix(),
	}

	return fileInfo, nil

}

// SetSecretKey update bucket secret key
func (h SwiftHandler) SetSecretKey(goz GozContext, bucket string) error {
	h.Init(goz)

	updateOpts := accounts.UpdateOpts{
		TempURLKey: goz.Config.Storage.Openstack.Secret,
	}

	storeClient, storeErr := openstack.NewObjectStorageV1(h.provider, gophercloud.EndpointOpts{
		Region:       goz.Config.Storage.Openstack.Region,
		Availability: gophercloud.AvailabilityPublic,
	})
	if storeErr != nil {
		log.Error().Err(storeErr).Msg("auth failed")
		return storeErr
	}

	accountRes := accounts.Update(storeClient, updateOpts)
	if accountRes.Err != nil {
		log.Error().Err(accountRes.Err).Msg("swift account settings failed")
	}
	return nil
}

// CreateBucket creates a bucket
// Account will have set the secret key
func (h SwiftHandler) CreateBucket(goz GozContext, bucket string) error {
	h.Init(goz)
	// CORS ?: X-Container-Meta-Access-Control-Allow-Origin: http://gozilla.url
	opts := containers.CreateOpts{
		Metadata: map[string]string{
			"owner":                       goz.User.ID,
			"Access-Control-Allow-Origin": "*",
		},
	}

	updateOpts := accounts.UpdateOpts{
		TempURLKey: goz.Config.Storage.Openstack.Secret,
	}

	storeClient, storeErr := openstack.NewObjectStorageV1(h.provider, gophercloud.EndpointOpts{
		Region:       goz.Config.Storage.Openstack.Region,
		Availability: gophercloud.AvailabilityPublic,
	})
	if storeErr != nil {
		log.Error().Err(storeErr).Msg("bucket creation failed")
		return storeErr
	}

	accountRes := accounts.Update(storeClient, updateOpts)
	if accountRes.Err != nil {
		log.Error().Err(accountRes.Err).Msg("swift account settings failed")
	}

	contRes := containers.Create(storeClient, bucket, opts)
	if contRes.Err != nil {
		log.Error().Err(accountRes.Err).Str("bucket", bucket).Msg("swift container creation failed")
	}
	contRes = containers.Create(storeClient, bucket+"_segments", opts)
	if contRes.Err != nil {
		log.Error().Err(accountRes.Err).Str("bucket", bucket+"_segments").Msg("swift container creation failed")
	}
	return nil
}

// CreateRepo creates repository in bucket
func (h SwiftHandler) CreateRepo(goz GozContext, bucket string, repo string) error {
	return nil
}

// SetQuota update quota for bucket according to plan
func (h SwiftHandler) SetQuota(goz GozContext, bucket string, plan PlanInfo) error {
	if plan.Quota.Size == "" {
		log.Debug().Str("plan", plan.ID).Msg("no quota")
		return nil
	}
	h.Init(goz)
	storeClient, storeErr := openstack.NewObjectStorageV1(h.provider, gophercloud.EndpointOpts{
		Region:       goz.Config.Storage.Openstack.Region,
		Availability: gophercloud.AvailabilityPublic,
	})
	if storeErr != nil {
		log.Error().Err(storeErr).Msg("auth failed")
		return storeErr
	}

	quota, quotaErr := humanize.ParseBytes(plan.Quota.Size)
	if quotaErr != nil {
		log.Error().Err(quotaErr).Str("plan", plan.ID).Msg("invalid quota")
		return quotaErr
	}

	updateOpts := containers.UpdateOpts{
		// X-Container-Meta-Quota-Bytes.
		Metadata: map[string]string{
			"Quota-Bytes": fmt.Sprintf("%d", quota),
		},
	}
	r := containers.Update(storeClient, bucket, updateOpts)
	if r.Err != nil {
		log.Error().Err(r.Err).Str("plan", plan.ID).Msg("failed to update quota")
	}
	return r.Err
}

// GetQuota returns used quota for bucket
func (h SwiftHandler) GetQuota(goz GozContext, bucket string) (int64, error) {
	h.Init(goz)
	storeClient, storeErr := openstack.NewObjectStorageV1(h.provider, gophercloud.EndpointOpts{
		Region:       goz.Config.Storage.Openstack.Region,
		Availability: gophercloud.AvailabilityPublic,
	})
	if storeErr != nil {
		log.Error().Err(storeErr).Msg("auth failed")
		return 0, storeErr
	}
	getOpts := containers.GetOpts{}
	r := containers.Get(storeClient, bucket, getOpts)
	headers, headersErr := r.Extract()
	if headersErr != nil {
		return 0, headersErr
	}
	return headers.BytesUsed, nil
}

// end swift handler
